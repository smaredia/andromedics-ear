-- -----------------------------------------------------
-- Table store
-- -----------------------------------------------------
DROP TABLE IF EXISTS store CASCADE;

CREATE  TABLE  store (
  id SERIAL ,
  name VARCHAR(255) NOT NULL ,
  street1 TEXT NOT NULL ,
  street2 TEXT NULL ,
  city VARCHAR(255) NOT NULL ,
  state VARCHAR(255) NOT NULL ,
  country VARCHAR(255) NOT NULL ,
  phone_no VARCHAR(20) NOT NULL ,
  fax VARCHAR(20) NULL ,
  pincode VARCHAR(45) NOT NULL ,
  website VARCHAR(255) NULL ,
  email VARCHAR(255) NOT NULL ,
  created_on TIMESTAMP NOT NULL ,
  subscription_date TIMESTAMP NOT NULL ,
  active INT NOT NULL ,
  PRIMARY KEY (id) )
;


-- -----------------------------------------------------
-- Table branch
-- -----------------------------------------------------
DROP TABLE IF EXISTS branch CASCADE;

CREATE  TABLE  branch (
  id SERIAL,
  opening_timing VARCHAR(50) NULL ,
  closing_timing VARCHAR(50) NULL ,
  name VARCHAR(255) NOT NULL ,
  street1 VARCHAR(255) NOT NULL ,
  street2 VARCHAR(255) NULL ,
  city VARCHAR(255) NOT NULL ,
  state VARCHAR(255) NOT NULL ,
  country VARCHAR(255) NOT NULL ,
  phone_no VARCHAR(20) NOT NULL ,
  fax VARCHAR(20),
  pincode VARCHAR(20) NOT NULL ,
  email VARCHAR(255) NOT NULL ,
  longitude VARCHAR(45) NOT NULL ,
  latitude VARCHAR(45) NOT NULL ,
  created_on TIMESTAMP NOT NULL ,
  active INT NOT NULL ,
  store_id INT NOT NULL ,
  PRIMARY KEY (id) ,
  CONSTRAINT fk_branch_store
    FOREIGN KEY (store_id)
    REFERENCES store (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
;


-- -----------------------------------------------------
-- Table person
-- -----------------------------------------------------
DROP TABLE IF EXISTS person CASCADE;

CREATE  TABLE  person (
  id SERIAL,
  first_name VARCHAR(255) NOT NULL ,
  middle_name VARCHAR(255) NULL ,
  last_name VARCHAR(255) NOT NULL ,
  phone_no VARCHAR(20) NULL,
  address TEXT NULL ,
  username VARCHAR(255) NOT NULL ,
  password TEXT NOT NULL ,
  email VARCHAR(255) NULL ,
  date_of_birth DATE NULL ,
  phone_type VARCHAR(100) NULL ,
  store_id INT NULL,
  branch_id INT NULL ,
  role VARCHAR(45) NOT NULL ,
  person_type VARCHAR(50) NOT NULL,
  PRIMARY KEY (id) ,
  UNIQUE(username),
  CONSTRAINT fk_person_branch
    FOREIGN KEY (branch_id)
    REFERENCES branch (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_person_store
    FOREIGN KEY (store_id)
    REFERENCES store (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
;

-- -----------------------------------------------------
-- Table drug_type
-- -----------------------------------------------------
DROP TABLE IF EXISTS drug_type CASCADE;

CREATE  TABLE  drug_type (
  id SERIAL,
  name VARCHAR(200) NOT NULL ,
  PRIMARY KEY (id) ,
  UNIQUE(name) )
;


-- -----------------------------------------------------
-- Table drug_manufacturer
-- -----------------------------------------------------
DROP TABLE IF EXISTS drug_manufacturer CASCADE;

CREATE  TABLE  drug_manufacturer (
  id SERIAL,
  name VARCHAR(255) NOT NULL ,
  PRIMARY KEY (id) ,
  UNIQUE(name) )
;


-- -----------------------------------------------------
-- Table drug
-- -----------------------------------------------------
DROP TABLE IF EXISTS drug CASCADE;

CREATE  TABLE  drug (
  id SERIAL,
  name VARCHAR(255) NOT NULL ,
  drug_type_id INT NOT NULL ,
  drug_manufacturer_id INT NOT NULL ,
  quantity VARCHAR(255) NOT NULL ,
  price DECIMAL NOT NULL ,
  drug_power VARCHAR(45) NULL ,
  constituents TEXT NULL ,
  diseases TEXT NULL ,
  code VARCHAR(255) NOT NULL ,
  PRIMARY KEY (id) ,
  UNIQUE(name) ,
  UNIQUE(code) ,
  CONSTRAINT fk_drug_drug_type1
    FOREIGN KEY (drug_type_id )
    REFERENCES drug_type (id )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_drug_drug_manufacturer1
    FOREIGN KEY (drug_manufacturer_id )
    REFERENCES drug_manufacturer (id )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
;


-- -----------------------------------------------------
-- Table stock
-- -----------------------------------------------------
DROP TABLE IF EXISTS stock CASCADE;

CREATE  TABLE  stock (
  id SERIAL,
  branch_id INT NOT NULL ,
  drug_id INT NOT NULL ,
  quantity INT NOT NULL ,
  reorder_level INT NOT NULL DEFAULT 0 ,
  PRIMARY KEY (id) ,
  CONSTRAINT fk_stock_branch1
    FOREIGN KEY (branch_id )
    REFERENCES branch (id )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_stock_drug1
    FOREIGN KEY (drug_id )
    REFERENCES drug (id )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
;


-- -----------------------------------------------------
-- Table invoice
-- -----------------------------------------------------
DROP TABLE IF EXISTS invoice CASCADE;

CREATE  TABLE  invoice (
  id SERIAL,
  branch_id INT NOT NULL ,
  total_amount DECIMAL NOT NULL ,
  invoice_no VARCHAR(45) NOT NULL ,
  sales_via INT NOT NULL,
  created_on TIMESTAMP NOT NULL,
  PRIMARY KEY (id) ,
  UNIQUE(invoice_no),
	  CONSTRAINT fk_invoice_branch1
    FOREIGN KEY (branch_id )
    REFERENCES branch (id )
    ON DELETE CASCADE
    ON UPDATE CASCADE
	 )
;


-- -----------------------------------------------------
-- Table invoice_product
-- -----------------------------------------------------
DROP TABLE IF EXISTS invoice_product CASCADE;

CREATE  TABLE  invoice_product (
  id SERIAL,
  quantity INT NOT NULL ,
  drug_id INT NOT NULL ,
  unit_price DECIMAL NOT NULL ,
  invoice_id INT NOT NULL ,
  PRIMARY KEY (id) ,
  CONSTRAINT fk_invoice_products_drug1
    FOREIGN KEY (drug_id )
    REFERENCES drug (id )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_invoice_products_invoice1
    FOREIGN KEY (invoice_id )
    REFERENCES invoice (id )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
;


-- -----------------------------------------------------
-- Table stock_block
-- -----------------------------------------------------
DROP TABLE IF EXISTS stock_block CASCADE;

CREATE  TABLE  stock_block (
  id SERIAL,
  branch_id INT NOT NULL ,
  person_id INT NOT NULL ,
  code VARCHAR(45) NOT NULL ,
  flag INT NOT NULL ,
  created_on TIMESTAMP NOT NULL ,
  blocked_time TIMESTAMP NOT NULL ,
  PRIMARY KEY (id) ,
  UNIQUE(code) ,  
CONSTRAINT fk_stock_block_branch1
    FOREIGN KEY (branch_id )
    REFERENCES branch (id )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_stock_block_person1
    FOREIGN KEY (person_id )
    REFERENCES person (id )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
;


-- -----------------------------------------------------
-- Table stock_block_product
-- -----------------------------------------------------
DROP TABLE IF EXISTS stock_block_product CASCADE;

CREATE  TABLE  stock_block_product (
  id SERIAL,
  quantity INT NOT NULL ,
  drug_id INT NOT NULL ,
  stock_block_id INT NOT NULL ,
  PRIMARY KEY (id) ,
  CONSTRAINT fk_stock_block_product_drug1
    FOREIGN KEY (drug_id )
    REFERENCES drug (id )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_stock_block_product_stock_block1
    FOREIGN KEY (stock_block_id )
    REFERENCES stock_block (id )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
;

insert into person values (nextval('person_id_seq'), 'Sahir', 'Zulfikar', 'Maredia', '9994886268','Mumbai','sahir',md5('sahir123'),'sahirzm@gmail.com','1986-12-15',null,null, null,'admin','Admin');
insert into person values (nextval('person_id_seq'), 'Chiranjit', '', 'Moulik', '9994886268','Mumbai','chiranjit',md5('chiranjit123'),'chiranjit.moulik@gmail.com','1986-12-15',null,null, null,'admin','Admin');
insert into person values (nextval('person_id_seq'), 'Gaurav', '', 'Patel', '9994886268','Mumbai','gaurav',md5('gaurav123'),'ptwo.patel@gmail.com','1986-12-15',null,null, null,'customer','Customer');
insert into person values (nextval('person_id_seq'), 'Ankit', '', 'Pandey', '9994886268','Mumbai','ankit',md5('ankit123'),'ankit.pandey@gmail.com','1986-12-15',null,null, null,'customer','Customer');
insert into person values (nextval('person_id_seq'), 'Satya', '', 'Mishra', '9994886268','Mumbai','satya',md5('satya'),'smsatya1@gmail.com','1986-12-15',null,null, null,'employee','Employee');
insert into drug_manufacturer (name, id) values ('Galxo', nextval('drug_manufacturer_id_seq'));
insert into drug_manufacturer (name, id) values ('Cipla', nextval('drug_manufacturer_id_seq'));
insert into drug_manufacturer (name, id) values ('Ranbaxy', nextval('drug_manufacturer_id_seq'));
insert into drug_manufacturer (name, id) values ('Himalaya', nextval('drug_manufacturer_id_seq'));
insert into drug_type (name) values ('Tablet');
insert into drug_type (name) values ('Syrup');
insert into drug_type (name) values ('Ointment');
insert into drug_type (name) values ('Chewable');
insert into drug_type (name) values ('Injection');

