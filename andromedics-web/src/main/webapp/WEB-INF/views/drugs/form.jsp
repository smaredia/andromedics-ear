<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<form id="drugForm">
	<fieldset>
		<legend>
			<fmt:message key="DRUG_DETAILS" />
		</legend>
		<div class="row-fluid">
			<c:if test="${drug.id gt 0}">
				<div class="span4">
					<label for="id">
						<fmt:message key="DRUG_ID_LBL" />
					</label>
					${drug.id }
					<input type="hidden" id="id" name="id" value="${drug.id }">
				</div>
			</c:if>
			<div class="span4">
				<label for="name">
					<fmt:message key="DRUG_NAME_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="name" name="name" class="span12"
					value="${drug.name }">
			</div>
			<div class="span4">
				<label for="code">
					<fmt:message key="DRUG_CODE_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="code" name="code" class="span12"
					value="${drug.code }">
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6">
				<label for="drugtype">
					<fmt:message key="DRUG_TYPE_LBL" />
					<span class="required">*</span>
				</label>
				<select id="drugType" name="drugType" class="span12">
					<option value="">
						<fmt:message key="SELECT_DRUG_TYPE" />
					</option>
					<c:forEach items="${drugTypes}" var="drugType">
						<option value="${drugType.id}"
							<c:if test="${drug.drugType.id eq drugType.id}">selected="selected"</c:if>>
							<c:out value="${drugType.name }" />
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="span6">
				<label for="price">
					<fmt:message key="DRUG_PRICE_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="price" name="price" class="span12"
					value="${drug.price }">
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6">
				<label for="quantity">
					<fmt:message key="DRUG_QUANTITY_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="quantity" name="quantity" class="span12"
					value="${drug.quantity }">
			</div>
			<div class="span6">
				<label for="drugPower">
					<fmt:message key="DRUG_POWER_LBL" />
				</label>
				<input type="text" id="drugPower" name="drugPower" class="span12"
					value="${drug.drugPower }">
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>
			<fmt:message key="DRUG_OTHER_DETAILS" />
		</legend>
		<div class="row-fluid">
			<div class="span6">
				<label for="disease">
					<fmt:message key="DRUG_DISEASE_LBL" />
				</label>
				<textarea id="diseases" name="diseases" class="span12">${drug.diseases }</textarea>
			</div>
			<div class="span6">
				<label for="constituents">
					<fmt:message key="DRUG_CONSTITUENT_LBL" />
				</label>
				<textarea id="constituents" name="constituents" class="span12">${drug.constituents }</textarea>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>
			<fmt:message key="DRUG_MANUFATURER_DETAILS" />
		</legend>
		<div class="row-fluid">
			<div class="span6">
				<label for="drugmanufacturer">
					<fmt:message key="DRUG_MANUFACTURER_LBL" />
					<span class="required">*</span>
				</label>
				<select id="drugManufacturer" name="drugManufacturer" class="span12">
					<option value="">
						<fmt:message key="SELECT_MANUFACTURER" />
					</option>
					<c:forEach items="${manufacturers}" var="drugManufacturer">
						<option value="${drugManufacturer.id}"
							<c:if test="${drug.drugManufacturer.id eq drugManufacturer.id}">selected="selected"</c:if>>
							<c:out value="${drugManufacturer.name }" />
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
	</fieldset>
	<button type="button" id="submit" class="btn btn-primary">
		<fmt:message
			key="${drug.id gt 0 ? 'BTN_UPDATE_LBL' : 'BTN_SAVE_LBL' }" />
	</button>
	<button type="button" id="cancel" class="btn">
		<fmt:message key="BTN_CANCEL_LBL" />
	</button>
</form>
<script type="text/javascript">
	$(function() {
		<c:choose>
		<c:when test="${drug.id gt 0 }">
		changeHeading('<fmt:message key="UPDATE_DRUG_HEAD"/> #${drug.id}');
		changeTitle('<fmt:message key="UPDATE_DRUG_TITLE"/> #${drug.id}');
		</c:when>
		<c:otherwise>
		changeHeading('<fmt:message key="CREATE_DRUG_HEAD"/>');
		changeTitle('<fmt:message key="CREATE_DRUG_TITLE"/>');
		</c:otherwise>
		</c:choose>
		$('#submit').click(function() {
			<c:choose>
			<c:when test="${drug.id gt 0 }">
			submitForm("#drugForm", "/drugs/edit.do", "#content");
			</c:when>
			<c:otherwise>
			submitForm("#drugForm", "/drugs/create.do", "#content");
			</c:otherwise>
			</c:choose>
		});
	});
</script>