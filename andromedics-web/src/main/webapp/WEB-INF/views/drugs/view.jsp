<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<table class="table table-bordered">
	<tr class="success">
		<td colspan="4"><fmt:message key="DRUG_DETAILS" /></td>
	</tr>
</table>
<div class="row-fluid">
	<button type="button" id="edit" class="btn"
		onclick="loadForm('/drugs/edit.do','#content','drugId=${drug.id}')">
		<fmt:message key="BTN_EDIT_LBL" />
	</button>
	<button type="button" id="delete" class="btn warn"
		onclick="loadForm('/drugs/delete.do','#content','drugId=${drug.id }')">
		<fmt:message key="BTN_DELETE_LBL" />
	</button>
</div>
<script type="text/javascript">
	$(function() {
		changeHeading('<fmt:message key="VIEW_DRUG_HEAD"/>');
		changeTitle('<fmt:message key="VIEW_DRUG_TITLE"/> #${drug.id}');
	});
</script>