<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<h1>Find Chemist Shops Near you</h1>
<div id="gMap" style="height: 400px;width:100%">Space for Map (Google)</div>
<script type="text/javascript">
	$(function(){
		//centering Calgary Map
	        var latlng = new google.maps.LatLng(51.045495,-114.057369);
	        var myOptions = {
	            zoom: 11,
	            center: latlng,
	            mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
	        
	        //creating google map in map_canvas
	        var map = new google.maps.Map(document.getElementById("gMap"),
	        myOptions);
 
	});
</script>