<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<form id="manufacturerForm">
	<fieldset>
		<legend>
			<fmt:message key="DRUG_MANUFACTURER_DETAILS" />
		</legend>
		<div class="row-fluid">
			<c:if test="${manufacturer.id gt 0}">
				<div class="span6">
					<label for="id">
						<fmt:message key="DRUG_MANUFACTURER_ID_LBL" />
					</label>
					${manufacturer.id }
					<input type="hidden" id="id" name="id" value="${manufacturer.id }">
				</div>
			</c:if>
			<div class="span6">
				<label for="name">
					<fmt:message key="DRUG_MANUFACTURER_NAME_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="name" name="name" class="span12"
					value="${manufacturer.name }">
			</div>
		</div>
	</fieldset>
	<div class="row-fluid">
		<button type="button" id="submit" class="btn btn-primary">
			<fmt:message
				key="${manufacturer.id gt 0 ? 'BTN_UPDATE_LBL' : 'BTN_SAVE_LBL' }" />
		</button>
		<button type="button" id="cancel" class="btn">
			<fmt:message key="BTN_CANCEL_LBL" />
		</button>
	</div>
</form>
<script type="text/javascript">
	$(function() {
		changeHeading('<fmt:message key="CREATE_DRUG_MANUFACTURER_HEAD"/>');
		changeTitle('<fmt:message key="CREATE_DRUG_MANUFACTURER_TITLE"/>');
		$('#submit').click(
				function() {
					<c:choose>
					<c:when test="${manufacturer.id gt 0 }">
					submitForm("#manufacturerForm", "/manufacturers/edit.do",
							"#content");
					</c:when>
					<c:otherwise>
					submitForm("#manufacturerForm", "/manufacturers/create.do",
							"#content");
					</c:otherwise>
					</c:choose>

				});
	});
</script>

