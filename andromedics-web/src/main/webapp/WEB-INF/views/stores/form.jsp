<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<form id="storeForm">
	<fieldset>
		<legend>
			<fmt:message key="STORE_DETAILS" />
		</legend>
		<div class="row-fluid">
			<c:if test="${store.id gt 0}">
				<div class="span6">
					<label for="id">
						<fmt:message key="STORE_ID_LBL" />
						<span class="required">*</span>
					</label>
					<span class="span12">${store.id }</span>
					<input type="hidden" id="id" name="id" value="${store.id }">
				</div>
			</c:if>
			<div class="span6">
				<label for="name">
					<fmt:message key="STORE_NAME_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="name" name="name" class="span12"
					value="${store.name }">
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>
			<fmt:message key="STORE_ADDRESS_DETAILS" />
		</legend>
		<div class="row-fluid">
			<div class="span6">
				<label for="street1">
					<fmt:message key="STORE_STREET1_LBL" />
					<span class="required">*</span>
				</label>
				<textarea id="street1" name="street1" class="span12">${store.street1 }</textarea>
			</div>
			<div class="span6">
				<label for="street2">
					<fmt:message key="STORE_STREET2_LBL" />
				</label>
				<textarea type="text" id="street2" name="street2" class="span12">${store.street2 }</textarea>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span4">
				<label for="city">
					<fmt:message key="STORE_CITY_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="city" name="city" class="span12"
					value="${store.city }">
			</div>
			<div class="span4">
				<label for="state">
					<fmt:message key="STORE_STATE_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="state" name="state" class="span12"
					value="${store.state }">
			</div>
			<div class="span4">
				<label for="country">
					<fmt:message key="STORE_COUNTRY_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="country" name="country" class="span12"
					value="${store.country }">
			</div>
		</div>
		<div class="row-fluid">
			<div class="span4">
				<label for="pincode">
					<fmt:message key="STORE_PINCODE_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="pincode" name="pincode" class="span12"
					value="${store.pincode }">
			</div>
			<div class="span4">
				<label for="phoneNo">
					<fmt:message key="STORE_PHONE_NO_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="phoneNo" name="phoneNo" class="span12"
					value="${store.phoneNo }">
			</div>
			<div class="span4">
				<label for="fax">
					<fmt:message key="STORE_FAX_LBL" />
				</label>
				<input type="text" id="fax" name="fax" class="span12"
					value="${store.fax }">
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6">
				<label for="email">
					<fmt:message key="STORE_EMAIL_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="email" name="email" class="span12"
					value="${store.email }">
			</div>
			<div class="span6">
				<label for="website">
					<fmt:message key="STORE_WEBSITE_LBL" />
				</label>
				<input type="text" id="website" name="website" class="span12"
					value="${store.website }">
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>
			<fmt:message key="STORE_SUBSCRIPTION_DETAILS" />
		</legend>
		<div class="row-fluid">
			<div class="span6">
				<label for="subscriptionDate">
					<fmt:message key="STORE_SUBSCRIPTION_DATE_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="subscriptionDate" name="subscriptionDate"
					class="span12"
					value='<fmt:formatDate value="${store.subscriptionDate }" pattern="yyyy-MM-dd" />'>
			</div>

			<c:if test="${store.id gt 0 }">
				<div class="span6">
					<label for="active">
						<fmt:message key="STORE_ACTIVE_LBL" />
						<span class="required">*</span>
					</label>
					<select id="active" name="active" class="span12">
						<option value="1" ${store.active eq 'ACTIVE' ? 'selected' : ''}>
							<fmt:message key="ACTIVE" />
						</option>
						<option value="0" ${store.active eq 'INACTIVE' ? 'selected' : ''}>
							<fmt:message key="INACTIVE" />
						</option>
					</select>
				</div>
			</c:if>
		</div>
	</fieldset>
	<div class="row-fluid">
		<button type="button" id="submit" class="btn btn-primary"
			href="javascript:;">
			<fmt:message
				key="${store.id gt 0 ? 'BTN_UPDATE_LBL' : 'BTN_SAVE_LBL' }" />
		</button>
		<button type="button" id="cancel" class="btn" href="javascript:;"
			onclick="loadForm('/stores/list.do','#content')">
			<fmt:message key="BTN_CANCEL_LBL" />
		</button>
	</div>
</form>
<script type="text/javascript">
	$(function() {
		changeHeading('<fmt:message key="CREATE_STORE_HEAD"/>');
		changeTitle('<fmt:message key="CREATE_STORE_TITLE"/>');
		var subscriptionDate = $('#subscriptionDate').datepicker({
			format : 'yyyy-mm-dd'
		}).on('show', function(ev) {
			$('#subscriptionDate').attr("disabled", "disabled");
		}).on('changeDate', function(ev) {
			$('#subscriptionDate').datepicker("hide");
			$('#subscriptionDate').removeAttr("disabled");
		}).on('hide', function(ev) {
			$('#subscriptionDate').removeAttr("disabled");
		});
		$('#submit').click(function() {
			<c:choose>
			<c:when test="${store.id gt 0 }">
			console.log("Calling");
			submitForm("#storeForm", "/stores/edit.do", "#content");
			</c:when>
			<c:otherwise>
			submitForm("#storeForm", "/stores/create.do", "#content");
			</c:otherwise>
			</c:choose>
		});
	});
</script>
