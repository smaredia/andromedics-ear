<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div id="myCarousel" class="carousel slide" style="margin-top: -22px">
	<div class="carousel-inner">
		<div class="item active">
			<img src="${baseUrl}/resources/img/slide-01.jpg" alt="" />

			<div class="container">
				<div class="carousel-caption">
					<h1>
						<fmt:message key="SLIDER1_HEAD" />
					</h1>

					<p class="lead">
						<fmt:message key="SLIDER1_BODY" />
					</p>
					<a href="${baseUrl}/persons/signup.do" class="btn btn-large btn-primary">
						<fmt:message key="BTN_SIGNUP_LBL" />
					</a>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="${baseUrl}/resources/img/slide-03.jpg" alt="" />

			<div class="container">
				<div class="carousel-caption">
					<h1>
						<fmt:message key="SLIDER2_HEAD" />
					</h1>

					<p class="lead">
						<fmt:message key="SLIDER2_BODY" />
					</p>
					<a class="btn btn-large btn-primary"
						href="${baseUrl}/contactus.jsp"><fmt:message
							key="CONTACTUS_LBL" /></a>
				</div>
			</div>
		</div>
	</div>
	<a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
	<a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
</div>
<script type="text/javascript">
	$(function() {
		$('#myCarousel').carousel({
			interval : 5000
		});
	});
</script>