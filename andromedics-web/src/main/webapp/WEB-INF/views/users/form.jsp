<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<form id="employeeForm">
	<fieldset>
		<legend>
			<fmt:message key="PERSON_DETAILS" />
		</legend>
		<div class="row-fluid">
			<c:if test="${employee.id gt 0}">
				<div class="span6">
					<label for="id"> <fmt:message key="PERSON_ID_LBL" /> <span
						class="required">*</span>
					</label> <span class="span12">${employee.id }</span> <input type="hidden"
						id="id" name="id" value="${employee.id }">
				</div>
			</c:if>
		</div>
		<div class="row-fluid">
			<div class="span4">
				<label for="firstname"> <fmt:message
						key="PERSON_FIRSTNAME_LBL" /> <span class="required">*</span>
				</label> <input type="text" id="firstName" name="firstName" class="span12"
					value="${employee.firstName }">
			</div>
			<div class="span4">
				<label for="middlename"> <fmt:message
						key="PERSON_MIDDLENAME_LBL" />
				</label> <input type="text" id="middleName" name="middleName" class="span12"
					value="${employee.middleName }">
			</div>
			<div class="span4">
				<label for="lastname"> <fmt:message
						key="PERSON_LASTNAME_LBL" /> <span class="required">*</span>
				</label> <input type="text" id="lastName" name="lastName" class="span12"
					value="${employee.lastName }">
			</div>
		</div>
		<div class="row-fluid">
			<div class="span4">
				<label for="dateOfBirth"> <fmt:message key="PERSON_DOB_LBL" />
				</label> <input type="text" id="dateOfBirth" name="dateOfBirth"
					class="span12" value="${employee.dateOfBirth }">
			</div>
			<div class="span4">
				<label for="email"> <fmt:message key="PERSON_EMAIL_LBL" />
				</label> <input type="text" id="email" name="email" class="span12"
					value="${employee.email }">
			</div>
			<div class="span4">
				<label for="phoneNo"> <fmt:message key="PERSON_PHONE_NO_LBL" />
				</label> <input type="text" id="phoneNo" name="phoneNo" class="span12"
					value="${employee.phoneNo }">
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6">
				<label for="address"> <fmt:message key="PERSON_ADDRESS_LBL" />
				</label>
				<textarea id="address" name="address" class="span12">${employee.address }</textarea>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>
			<fmt:message key="PERSON_LOGIN_DETAILS" />
		</legend>
		<div class="row-fluid">
			<div class="span6">
				<label for="name"> <fmt:message key="PERSON_USERNAME_LBL" />
					<span class="required">*</span>
				</label> <input type="text" id="username" name="username" class="span12"
					value="${employee.username }">
			</div>
			<div class="span6">
				<label for="password"> <fmt:message
						key="PERSON_PASSWORD_LBL" /> <span class="required">*</span>
				</label> <input type="password" id="password" name="password" class="span12"
					value="${employee.password }">
			</div>
		</div>
		<div class="row-fluid">
			<div class="span4">
				<label for="role"> <fmt:message key="PERSON_ROLE_LBL" /> <span
					class="required">*</span>
				</label> <select id="role" name="role" class="span12">
					<option value="">Select Role</option>
					<option value="employee"
						<c:if test="${employee.role eq 'employee'}">selected="selected"</c:if>>
						Employee</option>
					<option value="manager"
						<c:if test="${employee.role eq 'manager'}">selected="selected"</c:if>>
						Manager</option>
				</select>
			</div>
			<div class="span4">
				<label for="store"> <fmt:message key="EMPLOYEE_STORE_LBL" />
					<span class="required">*</span>
				</label> <select id="store" name="store" class="span12">
					<option value="">
						<fmt:message key="SELECT_STORE" />
					</option>
					<c:forEach items="${stores}" var="store">
						<option value="${store.id}"
							<c:if test="${employee.store.id eq store.id}">selected="selected"</c:if>>
							<c:out value="${store.name }" />
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="span4">
				<label for="branch"> <fmt:message key="EMPLOYEE_BRANCH_LBL" />
					<span class="required">*</span>
				</label> <select id="branch" name="branch" class="span12">
					<option value="">
						<fmt:message key="SELECT_BRANCH" />
					</option>
					<c:forEach items="${branches}" var="branch">
						<option value="${branch.id}"
							<c:if test="${employee.branch.id eq branch.id}">selected="selected"</c:if>>
							<c:out value="${branch.name }" />
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
	</fieldset>
	<button type="button" id="submit" class="btn btn-primary">
		<fmt:message
			key="${employee.id gt 0 ? 'BTN_UPDATE_LBL' : 'BTN_SAVE_LBL' }" />
	</button>
	<button type="button" id="cancel" class="btn">
		<fmt:message key="BTN_CANCEL_LBL" />
	</button>
</form>
<script type="text/javascript">
    $(function() {
	<c:choose>
	<c:when test="${employee.id gt 0 }">
	changeHeading('<fmt:message key="UPDATE_EMPLOYEE_HEAD"/> #${employee.id}');
	changeTitle('<fmt:message key="UPDATE_EMPLOYEE_TITLE"/> #${employee.id}');
	</c:when>
	<c:otherwise>
	changeHeading('<fmt:message key="CREATE_EMPLOYEE_HEAD"/>');
	changeTitle('<fmt:message key="CREATE_EMPLOYEE_TITLE"/>');
	</c:otherwise>
	</c:choose>
	$('#submit').click(function() {
		<c:choose>
		<c:when test="${employee.id gt 0 }">
		submitForm("#employeeForm", "/users/edit.do", "#content");
		</c:when>
		<c:otherwise>
		submitForm("#employeeForm", "/users/create.do", "#content");
		</c:otherwise>
		</c:choose>
	});
	var dateOfBirth = $('#dateOfBirth').datepicker({
	    format : 'yyyy-mm-dd'
	}).on('show', function(ev) {
	    $('#dateOfBirth').attr("disabled", "disabled");
	}).on('changeDate', function(ev) {
	    $('#dateOfBirth').datepicker("hide");
	    $('#dateOfBirth').removeAttr("disabled");
	}).on('hide', function(ev) {
	    $('#dateOfBirth').removeAttr("disabled");
	});
	$('#store')
		.change(
			function() {
			    $
				    .ajax({
					url : BASE_URL
						+ "/branches/getbystoreid?storeId="
						+ $('#store').val(),
					type : 'GET',
					dataType : 'json',
					success : function(data) {
					    var opt = "<option value=''><fmt:message key="SELECT_BRANCH"/></option>";
					    var i = 0;
					    $
						    .each(
							    data,
							    function(index,
								    value) {
								console
									.log(value);
								opt += "<option value='"+value.id+"'>"
									+ value.name
									+ "</option>";
							    });
					    $('#branch').html(opt);
					}
				    });
			});
    });
</script>