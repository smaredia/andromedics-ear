<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<form id="signupForm">
	<fieldset>
		<legend>
			<fmt:message key="PERSON_DETAILS" />
		</legend>
		<div class="row-fluid">
			<c:if test="${person.id gt 0}">
				<div class="span6">
					<label for="id">
						<fmt:message key="PERSON_ID_LBL" />
						<span class="required">*</span>
					</label>
					<span class="span12">${person.id }</span>
					<input type="hidden" id="id" name="id" value="${person.id }">
				</div>
			</c:if>
		</div>
		<div class="row-fluid">
			<div class="span4">
				<label for="firstname">
					<fmt:message key="PERSON_FIRSTNAME_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="firstname" name="firstname" class="span12"
					value="${person.firstname }">
			</div>
			<div class="span4">
				<label for="middlename">
					<fmt:message key="PERSON_MIDDLENAME_LBL" />
				</label>
				<input type="text" id="middlename" name="middlename" class="span12"
					value="${person.middlename }">
			</div>
			<div class="span4">
				<label for="lastname">
					<fmt:message key="PERSON_LASTNAME_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="lastname" name="lastname" class="span12"
					value="${person.lastname }">
			</div>
			</div>
			<div class="row-fluid">
			<div class="span4">
				<label for="dateOfBirth">
					<fmt:message key="PERSON_DOB_LBL" />
				</label>
				<input type="text" id="dateOfBirth" name="dateOfBirth" class="span12"
					value="${person.dateOfBirth }">
			</div>
			<div class="span4">
				<label for="email">
					<fmt:message key="PERSON_EMAIL_LBL" />
				</label>
				<input type="text" id="email" name="email" class="span12"
					value="${person.email }">
			</div>
			<div class="span4">
				<label for="phoneNo">
					<fmt:message key="PERSON_PHONE_NO_LBL" />
				</label>
				<input type="text" id="phoneNo" name="phoneNo" class="span12"
					value="${person.phoneNo }">
			</div>
			</div>
			<div class="row-fluid">
			<div class="span6">
				<label for="address">
					<fmt:message key="PERSON_ADDRESS_LBL" />
				</label>
				<textarea type="text" id="address" name="address" class="span12">${person.address }</textarea>
			</div>
			</div>
	</fieldset>
	<fieldset>
	<legend>
			<fmt:message key="PERSON_LOGIN_DETAILS" />
		</legend>		
			<div class="row-fluid">
			<div class="span6">
				<label for="name">
					<fmt:message key="PERSON_USERNAME_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="username" name="username" class="span12"
					value="${person.username }">
			</div>
			<div class="span6">
				<label for="password">
					<fmt:message key="PERSON_PASSWORD_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="password" name="password" class="span12"
					value="${person.password }">
			</div>
			</div>
			<div class="row-fluid">
			<div class="span6">
				<label for="role">
					<fmt:message key="PERSON_ROLE_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="role" name="role" class="span12"
					value="${person.role }">
			</div>
			</div>
	</fieldset>
		<button type="button" id="submit" class="btn btn-primary">
			<fmt:message
				key="${store.id gt 0 ? 'BTN_UPDATE_LBL' : 'BTN_SAVE_LBL' }" />
		</button>
		<button type="button" id="cancel" class="btn">
			<fmt:message key="BTN_CANCEL_LBL" />
		</button>
</form>
<script type="text/javascript">
	$(function() {
		changeHeading('<fmt:message key="CREATE_STORE_HEAD"/>');
		changeTitle('<fmt:message key="CREATE_STORE_TITLE"/>');
		$('#submit').click(function() {
			submitForm("#storeForm", "/stores/create.do", "#content");
		});
	});
</script>