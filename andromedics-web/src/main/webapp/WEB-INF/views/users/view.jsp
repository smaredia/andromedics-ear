<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<table class="table table-bordered">
	<tr class="success">
		<td colspan="4"><fmt:message key="PERSON_DETAILS" /></td>
	</tr>
	<tr>
		<td><fmt:message key="PERSON_ID_LBL" /></td>
		<td>${user.id }</td>
	</tr>
</table>

<div class="row-fluid">
	<button type="button" id="edit" class="btn"
		onclick="loadForm('/users/edit.do','#content','userId=${user.id}')">
		<fmt:message key="BTN_EDIT_LBL" />
	</button>
	<button type="button" id="delete" class="btn warn"
		onclick="loadForm('/users/delete.do','#content','userId=${user.id }')">
		<fmt:message key="BTN_DELETE_LBL" />
	</button>
</div>
<script type="text/javascript">
	$(function() {
		changeHeading('<fmt:message key="VIEW_PERSON_HEAD"/>');
		changeTitle('<fmt:message key="VIEW_PERSON_TITLE"/> #${user.id}');
	});
</script>