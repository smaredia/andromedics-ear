<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<table id="userList" class="table table-bordered dataTable">
	<thead>
		<tr>
			<th><fmt:message key="PERSON_ID_LBL" /></th>
			<th><fmt:message key="PERSON_FIRSTNAME_LBL" /></th>
			<th><fmt:message key="PERSON_LASTNAME_LBL" /></th>
			<th><fmt:message key="PERSON_EMAIL_LBL" /></th>
			<th><fmt:message key="PERSON_ROLE_LBL" /></th>
			<th><fmt:message key="ACTIONS" /></th>
		</tr>
	</thead>
	<tfoot>
		<tr class="success">
			<th><input type="text"
				value="<fmt:message key="PERSON_ID_LBL" />" /></th>
			<th><input type="text"
				value="<fmt:message key="PERSON_FIRSTNAME_LBL" />" /></th>
			<th><input type="text"
				value="<fmt:message key="PERSON_LASTNAME_LBL" />" /></th>
			<th><input type="text"
				value="<fmt:message key="PERSON_EMAIL_LBL" />" /></th>
			<th><select>
					<option value="">
						<fmt:message key="PERSON_ROLE_LBL" />
					</option>
					<option value="admin">
						<fmt:message key="PERSON_ROLE_ADMIN" />
					</option>
					<option value="customer">
						<fmt:message key="PERSON_ROLE_CUSTOMER" />
					</option>
					<option value="employee">
						<fmt:message key="PERSON_ROLE_EMPLOYEE" />
					</option>
					<option value="manager">
						<fmt:message key="PERSON_ROLE_MANAGER" />
					</option>
			</select></th>
			<th><fmt:message key="ACTIONS" /></th>
		</tr>
	</tfoot>
	<tbody>
	</tbody>
</table>

<script type="text/javascript">
    $(function() {

	var oTable = $('#userList')
		.dataTable(
			{
			    bProcessing : true,
			    bServerSide : true,
			    sAjaxSource : BASE_URL + "/users/list.do",
			    sDom : "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			    sPaginationType : "bootstrap"
			});

	var asInitVals = new Array();

	$("tfoot input").keyup(function() {
	    /* Filter on the column (the index) of this element */
	    oTable.fnFilter(this.value, $(this).parent().index());
	});

	$("tfoot select").change(function() {
	    /* Filter on the column (the index) of this element */
	    oTable.fnFilter(this.value, $(this).parent().index());
	});

	$("tfoot input").each(function(i) {
	    asInitVals[$(this).parent().index()] = this.value;
	    this.className = "search_init";
	});

	$("tfoot input").focus(function() {
	    if (this.className == "search_init") {
		this.className = "";
		this.value = "";
	    }
	});

	$("tfoot input").blur(function(i) {
	    if (this.value == "") {
		this.className = "search_init";
		this.value = asInitVals[$(this).parent().index()];
	    }
	});
	changeHeading('<fmt:message key="LIST_USERS_HEAD"/>');
	changeTitle('<fmt:message key="LIST_USERS_TITLE"/>');
    });
</script>