<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<button type="button" class="btn btn-navbar" data-toggle="collapse"
				data-target=".nav-collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="brand" href="${baseUrl}/"><fmt:message
					key="ANDROMEDICS" /></a>

			<c:if test="${currentUser.loggedIn}">
				<div class="navbar-text pull-right dropdown">

					<a class="dropdown-toggle" id="dLabel" role="button"
						data-toggle="dropdown" data-target="#" href="#"> <fmt:message
							key="WELCOME" /> ${currentUser.name}
					</a> <b class="caret"></b>

					<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
						<li><a href="#"><fmt:message key="MY_PROFILE" /></a></li>
						<li><a href="${baseUrl}/logout.do"><fmt:message
									key="BTN_LOGOUT_LBL" /></a></li>
					</ul>

				</div>
			</c:if>

			<div class="nav-collapse collapse">
				<tags:mainmenu menus="${mainMenu }" />

			</div>
		</div>
	</div>
</div>
<div style="height: 43px"></div>
<!--<div class="masthead">
        <h3 class="muted">Project name</h3>
        <div class="navbar">
          <div class="navbar-inner">
            <div class="container">
              <ul class="nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">Projects</a></li>
                <li><a href="#">Services</a></li>
                <li><a href="#">Downloads</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      -->
<script type="text/javascript">
	$(function() {
		$('a.dropdown-toggle').attr("data-toggle", "dropdown");
		$('.dropdown-toggle').dropdown();
	});
</script>