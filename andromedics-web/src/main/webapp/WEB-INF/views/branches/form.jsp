<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<form id="branchForm">
	<fieldset>
		<legend>
			<fmt:message key="BRANCH_DETAILS" />
		</legend>
		<div class="row-fluid">
			<c:if test="${branch.id gt 0}">
				<div class="span4">
					<label for="id">
						<fmt:message key="BRANCH_ID_LBL" />
					</label>
					${branch.id }
					<input type="hidden" id="id" name="id" value="${branch.id }">
				</div>
			</c:if>
			<div class="span4">
				<label for="name">
					<fmt:message key="BRANCH_NAME_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="name" name="name" class="span12"
					value="${branch.name }">
			</div>
			<div class="span4">
				<label for="store">
					<fmt:message key="BRANCH_STORE_LBL" />
					<span class="required">*</span>
				</label>
				<select id="store" name="store" class="span12">
					<option value="">Select Store</option>
					<c:forEach items="${stores}" var="store">
						<option value="${store.id}"
							<c:if test="${branch.store.id eq store.id}">selected="selected"</c:if>>
							<c:out value="${store.name }" />
						</option>
					</c:forEach>
				</select>
			</div>

		</div>
	</fieldset>
	<fieldset>
		<legend>
			<fmt:message key="BRANCH_ADDRESS_DETAILS" />
		</legend>
		<div class="row-fluid">
			<div class="span6">
				<label for="street1">
					<fmt:message key="BRANCH_STREET1_LBL" />
					<span class="required">*</span>
				</label>
				<textarea id="street1" name="street1" class="span12">${branch.street1 }</textarea>
			</div>
			<div class="span6">
				<label for="street2">
					<fmt:message key="BRANCH_STREET2_LBL" />
				</label>
				<textarea id="street2" name="street2" class="span12">${branch.street2 }</textarea>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span4">
				<label for="city">
					<fmt:message key="BRANCH_CITY_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="city" name="city" class="span12"
					value="${branch.city }">
			</div>
			<div class="span4">
				<label for="state">
					<fmt:message key="BRANCH_STATE_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="state" name="state" class="span12"
					value="${branch.state }">
			</div>
			<div class="span4">
				<label for="country">
					<fmt:message key="BRANCH_COUNTRY_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="country" name="country" class="span12"
					value="${branch.country }">
			</div>
		</div>
		<div class="row-fluid">
			<div class="span4">
				<label for="pincode">
					<fmt:message key="BRANCH_PINCODE_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="pincode" name="pincode" class="span12"
					value="${branch.pincode }">
			</div>
			<div class="span4">
				<label for="phoneNo">
					<fmt:message key="BRANCH_PHONE_NO_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="phoneNo" name="phoneNo" class="span12"
					value="${branch.phoneNo }">
			</div>
			<div class="span4">
				<label for="fax">
					<fmt:message key="BRANCH_FAX_LBL" />
				</label>
				<input type="text" id="fax" name="fax" class="span12"
					value="${branch.fax }">
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6">
				<label for="longitude">
				<fmt:message key="BRANCH_LONGITUDE_LBL" />
				<span class="required">*</span>
				</label>
				<input type="text" id="longitude" name="longitude" class="span12"
					value="${branch.longitude }">
			</div>
			<div class="span6">
				<label for="latitude"> 
				<fmt:message key="BRANCH_LATITUDE_LBL" />
				<span class="required">*</span></label>
				<input type="text" id="latitude" name="latitude" class="span12"
					value="${branch.latitude }">
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6">
				<label for="email">
					<fmt:message key="BRANCH_EMAIL_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="email" name="email" class="span12"
					value="${branch.email }">
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>
			<fmt:message key="BRANCH_TIMING_DETAILS" />
		</legend>
		<div class="row-fluid">
			<div class="span6">
				<label for="opening_time">
					<fmt:message key="BRANCH_OPENING_TIME_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="openingTiming" name="openingTiming"
					class="span12" value="${branch.openingTiming }">
			</div>
			<div class="span6">
				<label for="closing_time">
					<fmt:message key="BRANCH_CLOSING_TIME_LBL" />
					<span class="required">*</span>
				</label>
				<input type="text" id="closingTiming" name="closingTiming"
					class="span12" value="${branch.closingTiming }">
			</div>
		</div>
	</fieldset>
	<c:if test="${branch.id gt 0 }">
		<fieldset>
			<legend></legend>
			<div class="row-fluid">
				<div class="span6">
					<label for="active">
						<fmt:message key="BRANCH_ACTIVE_LBL" />
						<span class="required">*</span>
					</label>
					<select id="active" name="active" class="span12">
						<option value="1"
							${branch.active eq status.ACTIVE ? 'selected' : ''}>
							<fmt:message key="ACTIVE" />
						</option>
						<option value="0"
							${branch.active eq status.INACTIVE ? 'selected' : ''}>
							<fmt:message key="INACTIVE" />
						</option>
					</select>
				</div>
			</div>
		</fieldset>
	</c:if>
	<div class="row-fluid">
		<button type="button" id="submit" class="btn btn-primary"
			href="javascript:;">${branch.id gt 0 ? 'Update' : 'Save' }</button>
		<button type="button" id="cancel" class="btn" href="javascript:;"
			onclick="loadForm('/branches/list.do','#content')">Cancel</button>
	</div>
</form>
<script type="text/javascript">
	$(function() {
		<c:choose>
		<c:when test="${branch.id gt 0 }">
		changeHeading('Update Branch #${branch.id}');
		changeTitle('Update Branch #${branch.id} | Admin Panel');
		</c:when>
		<c:otherwise>
		changeHeading('Create New Branch');
		changeTitle('Create New Branch | Admin Panel');
		</c:otherwise>
		</c:choose>
		$('#submit').click(function() {
			<c:choose>
			<c:when test="${branch.id gt 0 }">
			submitForm("#branchForm", "/branches/edit.do", "#content");
			</c:when>
			<c:otherwise>
			submitForm("#branchForm", "/branches/create.do", "#content");
			</c:otherwise>
			</c:choose>
		});
	});
</script>