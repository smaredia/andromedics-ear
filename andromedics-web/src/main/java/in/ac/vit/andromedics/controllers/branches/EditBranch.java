package in.ac.vit.andromedics.controllers.branches;

import in.ac.vit.andromedics.entities.Branch;
import in.ac.vit.andromedics.entities.Status;
import in.ac.vit.andromedics.entities.Store;
import in.ac.vit.andromedics.exceptions.ValidationException;
import in.ac.vit.andromedics.service.BranchService;
import in.ac.vit.andromedics.service.StoreService;
import in.ac.vit.andromedics.utils.ParamParser;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class EditBranch
 */
@WebServlet("/branches/edit.do")
public class EditBranch extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	private BranchService branchService;
	@Inject
	private StoreService storeService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int branchId = p.getInt("branchId");
		Branch branch = this.branchService.getById(branchId);
		if (branch == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		} else {
			request.setAttribute("branch", branch);
			request.setAttribute("stores", this.storeService.getAll());
			request.getRequestDispatcher("/WEB-INF/views/branches/form.jsp")
					.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int branchId = p.getInt("id");
		Branch branch = this.branchService.getById(branchId);
		if (branch == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		branch.setActive(p.getInt("active") == 1 ? Status.ACTIVE : Status.INACTIVE);
		branch.setCity(p.getString("city"));
		branch.setClosingTiming(p.getString("closingTiming"));
		branch.setCountry(p.getString("country"));
		branch.setEmail(p.getString("email"));
		branch.setFax(p.getString("fax"));
		branch.setLatitude(p.getString("latitude"));
		branch.setLongitude(p.getString("longitude"));
		branch.setName(p.getString("name"));
		branch.setOpeningTiming(p.getString("openingTiming"));
		branch.setPhoneNo(p.getString("phoneNo"));
		branch.setPincode(p.getString("pincode"));
		branch.setState(p.getString("state"));
		int storeId = p.getInt("store");
		Store store = this.storeService.getById(storeId);
		branch.setStore(store);
		branch.setStreet1(p.getString("street1"));
		branch.setStreet2(p.getString("street2"));
		try {
			branch = this.branchService.update(branch);
			response.sendRedirect(getServletContext().getContextPath()
					+ "/branches/view.do?branchId=" + branch.getId());
		} catch (EJBException e) {
			if (e.getCause() instanceof ValidationException) {
				ValidationException ve = (ValidationException) e.getCause();
				response.getWriter().print(ve.getAsJSON());
				return;
			} else {
				e.printStackTrace();
			}
		}
	}

}
