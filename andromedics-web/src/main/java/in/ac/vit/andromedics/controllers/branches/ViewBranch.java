package in.ac.vit.andromedics.controllers.branches;

import in.ac.vit.andromedics.entities.Branch;
import in.ac.vit.andromedics.service.BranchService;
import in.ac.vit.andromedics.utils.ParamParser;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class ViewBranch
 */
@WebServlet("/branches/view.do")
public class ViewBranch extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	private BranchService branchService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int branchId = p.getInt("branchId");
		Branch branch = this.branchService.getById(branchId);
		if (branch == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		} else {
			request.setAttribute("branch", branch);
			request.getRequestDispatcher("/WEB-INF/views/branches/view.jsp")
					.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
