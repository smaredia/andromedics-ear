package in.ac.vit.andromedics.controllers.stores;

import in.ac.vit.andromedics.service.StoreService;
import in.ac.vit.andromedics.utils.ParamParser;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class DeleteStore
 */
@WebServlet("/stores/delete.do")
@RolesAllowed({ "admin" })
public class DeleteStore extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Inject
	private StoreService storeService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int storeId = p.getInt("storeId");
		if (storeId == 0) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		} else {
			this.getStoreService().delete(storeId);
			response.sendRedirect(getServletContext().getContextPath()
					+ "/stores/list.do");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

	protected StoreService getStoreService() {
		return this.storeService;
	}
}
