package in.ac.vit.andromedics.controllers.manufacturers;

import in.ac.vit.andromedics.entities.DrugManufacturer;
import in.ac.vit.andromedics.exceptions.ValidationException;
import in.ac.vit.andromedics.service.DrugManufacturerService;
import in.ac.vit.andromedics.utils.ParamParser;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class EditManufacturer
 */
@WebServlet("/manufacturers/edit.do")
public class EditManufacturer extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	private DrugManufacturerService drugManufacturerService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int manufacturerId = p.getInt("manufacturerId");
		DrugManufacturer manufacturer = this.drugManufacturerService
				.getById(manufacturerId);
		if (manufacturer == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		} else {
			request.setAttribute("manufacturer", manufacturer);
			request.getRequestDispatcher(
					"/WEB-INF/views/manufacturers/form.jsp").forward(request,
					response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int manufacturerId = p.getInt("id");
		DrugManufacturer drugManufacturer = this.drugManufacturerService
				.getById(manufacturerId);
		if (drugManufacturer == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		drugManufacturer.setName(p.getString("name"));
		try {
			drugManufacturer = this.drugManufacturerService
					.update(drugManufacturer);
			response.sendRedirect(getServletContext().getContextPath()
					+ "/manufacturers/view.do?manufacturerId="
					+ drugManufacturer.getId());
		} catch (EJBException e) {
			if (e.getCause() instanceof ValidationException) {
				ValidationException ve = (ValidationException) e.getCause();
				response.getWriter().print(ve.getAsJSON());
				return;
			} else {
				e.printStackTrace();
			}
		}
	}

}
