package in.ac.vit.andromedics.controllers;

import in.ac.vit.andromedics.security.AndroMedicsPrincipal;
import in.ac.vit.andromedics.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;

@WebServlet(urlPatterns = "/login.do")
public class LoginController extends HttpServlet {
	protected transient Logger log = Logger.getLogger(getClass());
	@Inject
	private UserService userService;
	@Inject
	private CurrentUser currentUser;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		log.debug("Rendering login page...");
		TilesHelper.render(request, response, "login");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if (!(StringUtils.isBlank(username) || StringUtils.isBlank(password))) {

			try {
				request.login(username, password);
				HttpSession session = request.getSession();
				currentUser.setUser(this.getUserService().authenticate(
						username, password));
				Principal p = request.getUserPrincipal();
				if (p instanceof AndroMedicsPrincipal) {
					log.info(((AndroMedicsPrincipal) p).getUser().fullName());
				}
				session.setAttribute("currentUser", currentUser.getUser());
				response.sendRedirect("homepage.do");
				return;
			} catch (Exception e) {
				log.error("Login failed for " + username + " with password = "
						+ password);
			}
		}
		request.setAttribute("loginError", "INV_CREDENTIALS");
		this.doGet(request, response);
	}

	protected UserService getUserService() {
		return userService;
	}

}
