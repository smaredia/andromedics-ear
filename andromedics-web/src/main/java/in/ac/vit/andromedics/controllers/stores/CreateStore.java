package in.ac.vit.andromedics.controllers.stores;

import in.ac.vit.andromedics.entities.Status;
import in.ac.vit.andromedics.entities.Store;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.exceptions.ValidationException;
import in.ac.vit.andromedics.service.StoreService;
import in.ac.vit.andromedics.utils.ParamParser;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;

/**
 * Servlet implementation class CreateStore
 */
@WebServlet("/stores/create.do")
@ServletSecurity(@HttpConstraint(rolesAllowed = { UserRoles.ADMIN }))
public class CreateStore extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Inject
	private StoreService storeService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Store store = new Store();
		store.setCountry("India");
		request.setAttribute("store", store);
		request.getRequestDispatcher("/WEB-INF/views/stores/form.jsp").forward(
				request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Store store = new Store();
		ParamParser p = new ParamParser(request);
		store.setName(p.getString("name"));
		store.setStreet1(p.getString("street1"));
		store.setStreet2(p.getString("street2"));
		store.setCity(p.getString("city"));
		store.setState(p.getString("state"));
		store.setCountry(p.getString("country"));
		store.setPhoneNo(p.getString("phoneNo"));
		store.setPincode(p.getString("pincode"));
		store.setFax(p.getString("fax"));
		store.setWebsite(p.getString("website"));
		store.setEmail(p.getString("email"));
		store.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		store.setActive(Status.ACTIVE);
		store.setSubscriptionDate(p.getDate("subscriptionDate"));
		try {
			store = this.getStoreService().save(store);
			response.sendRedirect(getServletContext().getContextPath()
					+ "/stores/view.do?storeId=" + store.getId());
		} catch (EJBException e) {
			if (e.getCause() instanceof ValidationException) {
				ValidationException ve = (ValidationException) e.getCause();
				response.getWriter().print(ve.getAsJSON());
				return;
			} else {
				e.printStackTrace();
			}
		}
	}

	protected StoreService getStoreService() {
		return this.storeService;
	}
}