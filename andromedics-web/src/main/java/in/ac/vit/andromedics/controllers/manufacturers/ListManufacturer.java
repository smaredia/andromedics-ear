package in.ac.vit.andromedics.controllers.manufacturers;

import in.ac.vit.andromedics.entities.DrugManufacturer;
import in.ac.vit.andromedics.service.DrugManufacturerService;
import in.ac.vit.andromedics.utils.ParamParser;
import org.codehaus.jackson.map.ObjectMapper;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet implementation class ListManufacturer
 */
@WebServlet("/manufacturers/list.do")
public class ListManufacturer extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	private DrugManufacturerService drugManufacturerService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int sEcho = p.getInt("sEcho", -1);
		if (sEcho == -1) {
			request.getRequestDispatcher(
					"/WEB-INF/views/manufacturers/list.jsp").forward(request,
					response);
		} else {
			// echo json stores list
			int first = p.getInt("iDisplayStart");
			int pageSize = p.getInt("iDisplayLength");
			int sortCol = p.getInt("iSortCol_0");
			String sortField = "";
			switch (sortCol) {
			case 0:
				sortField = "id";
				break;
			case 1:
				sortField = "name";
				break;
			default:
				sortField = "id";
			}
			String sortOrder = p.getString("sSortDir_0");
			Map<String, String> filters = new HashMap<>();

			String[] list = { "id", "name" };
			for (int i = 0; i < list.length; i++) {
				String value = p.getString("sSearch_" + i, null);
				if (value != null) {
					filters.put(list[i], value);
				}
			}
			List<DrugManufacturer> drugManufacturers = this.drugManufacturerService
					.get(first, pageSize, sortField, sortOrder, filters);
			int totalCount = this.drugManufacturerService.getCount();
			int totalFilteredCount = this.drugManufacturerService
					.getCount(filters);
			Map<String, Object> map = new HashMap<>();
			map.put("sEcho", sEcho);
			map.put("iTotalRecords", totalCount);
			map.put("iTotalDisplayRecords", totalFilteredCount);
			String[][] aaData = new String[drugManufacturers.size()][];
			int i = 0;
			for (DrugManufacturer drugManufacturer : drugManufacturers) {
				String[] sData = new String[3];
				sData[0] = drugManufacturer.getId().toString();
				sData[1] = drugManufacturer.getName();
				sData[2] = "<a href='javascript:;' onclick='loadForm(\"/manufacturers/view.do\",\"#content\",\"manufacturerId="
						+ drugManufacturer.getId()
						+ "\")' class='viewLink'>view</a>";
				sData[2] += " <a href='javascript:;' onclick='loadForm(\"/manufacturers/edit.do\",\"#content\",\"manufacturerId="
						+ drugManufacturer.getId()
						+ "\")' class='editLink'>edit</a>";
				sData[2] += " <a href='javascript:;' onclick='loadForm(\"/manufacturers/delete.do\",\"#content\",\"manufacturerId="
						+ drugManufacturer.getId()
						+ "\")' class='deleteLink'>delete</a>";
				aaData[i++] = sData;
			}
			map.put("aaData", aaData);
			ObjectMapper mapper = new ObjectMapper();
			mapper.writeValue(response.getWriter(), map);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
