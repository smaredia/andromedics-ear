package in.ac.vit.andromedics.controllers;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Arrays;

import static in.ac.vit.andromedics.entities.UserRoles.*;

@ApplicationScoped
public class ApplicationMenus {
	private MenuBar mainMenu = new MenuBar(null, "nav", null);
	private MenuBar sideMenu = new MenuBar(null, "nav nav-tabs nav-stacked",
			Arrays.asList(new String[] { ADMIN, MANAGER, EMPLOYEE, CUSTOMER }));

	public ApplicationMenus() {

		mainMenu.addMenu(new Menu("HOME_LBL", "/homepage.do", null));
		mainMenu.addMenu(new Menu("ABOUTUS_LBL", "/aboutus.do", null));
		mainMenu.addMenu(new Menu("CONTACTUS_LBL", "/contactus.do", null));
		mainMenu.addMenu(new Menu("BTN_SIGNUP_LBL", "/persons/signup.do",
				new ArrayList<>(Arrays.asList(new String[] { ANONYMOUS }))));

		// stores side menu
		Menu stores = new Menu("MANAGE_STORES", "/stores/list.do",
				Arrays.asList(new String[] { ADMIN }));
		sideMenu.addMenu(stores);
		MenuBar storesMenuBar = new MenuBar(null, "submenus",
				Arrays.asList(new String[] { ADMIN }));
		storesMenuBar.addMenu(new Menu("LIST_LBL", Arrays
				.asList(new String[] { ADMIN }),
				"loadForm('/stores/list.do','#content')"));
		storesMenuBar.addMenu(new Menu("CREATE_LBL", Arrays
				.asList(new String[] { ADMIN }),
				"loadForm('/stores/create.do','#content')"));
		stores.setMenuBar(storesMenuBar);

		// branch side menu
		Menu branches = new Menu("MANAGE_BRANCHES", "/branches/list.do",
				Arrays.asList(new String[] { ADMIN, MANAGER }));
		sideMenu.addMenu(branches);
		MenuBar branchesMenuBar = new MenuBar(null, "submenus",
				Arrays.asList(new String[] { ADMIN, MANAGER }));
		branchesMenuBar.addMenu(new Menu("LIST_LBL", Arrays
				.asList(new String[] { ADMIN, MANAGER }),
				"loadForm('/branches/list.do','#content')"));
		branchesMenuBar.addMenu(new Menu("CREATE_LBL", Arrays
				.asList(new String[] { ADMIN }),
				"loadForm('/branches/create.do','#content')"));
		branches.setMenuBar(branchesMenuBar);

		// drug manufacturer
		Menu manufacturers = new Menu("MANAGE_MANUFACTURERS",
				"/manufacturers/list.do", Arrays.asList(new String[] { ADMIN,
						MANAGER, EMPLOYEE }));
		sideMenu.addMenu(manufacturers);
		MenuBar manuMenuBar = new MenuBar(null, "submenus",
				Arrays.asList(new String[] { ADMIN, MANAGER, EMPLOYEE }));
		manuMenuBar.addMenu(new Menu("LIST_LBL", Arrays.asList(new String[] {
				ADMIN, MANAGER, EMPLOYEE }),
				"loadForm('/manufacturers/list.do','#content')"));
		manuMenuBar.addMenu(new Menu("CREATE_LBL", Arrays
				.asList(new String[] { ADMIN }),
				"loadForm('/manufacturers/create.do','#content')"));
		manufacturers.setMenuBar(manuMenuBar);

		// drug
		Menu drugs = new Menu("MANAGE_MEDICINES", "/drugs/list.do",
				Arrays.asList(new String[] { ADMIN, MANAGER, EMPLOYEE }));
		sideMenu.addMenu(drugs);
		MenuBar drugsMenuBar = new MenuBar(null, "submenus",
				Arrays.asList(new String[] { ADMIN, MANAGER, EMPLOYEE }));
		drugsMenuBar.addMenu(new Menu("LIST_LBL", Arrays.asList(new String[] {
				ADMIN, MANAGER, EMPLOYEE }),
				"loadForm('/drugs/list.do','#content')"));
		drugsMenuBar.addMenu(new Menu("CREATE_LBL", Arrays
				.asList(new String[] { ADMIN }),
				"loadForm('/drugs/create.do','#content')"));
		drugs.setMenuBar(drugsMenuBar);

		// users
		Menu users = new Menu("MANAGE_USERS", "/users/list.do",
				Arrays.asList(new String[] { ADMIN }));
		sideMenu.addMenu(users);
		MenuBar usersMenuBar = new MenuBar(null, "submenus",
				Arrays.asList(new String[] { ADMIN }));
		usersMenuBar.addMenu(new Menu("LIST_LBL", Arrays
				.asList(new String[] { ADMIN }),
				"loadForm('/users/list.do','#content')"));
		usersMenuBar.addMenu(new Menu("CREATE_EMPLOYEE_LBL", Arrays
				.asList(new String[] { ADMIN }),
				"loadForm('/users/create.do','#content')"));
		users.setMenuBar(usersMenuBar);
	}

	@Named("mainMenu")
	@Produces
	public MenuBar getMainMenu() {
		return this.mainMenu;
	}

	@Produces
	@Named("sideMenu")
	public MenuBar getSideMenu() {
		return this.sideMenu;
	}

}
