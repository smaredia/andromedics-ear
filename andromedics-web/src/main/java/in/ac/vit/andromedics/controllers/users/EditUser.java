package in.ac.vit.andromedics.controllers.users;

import in.ac.vit.andromedics.entities.Branch;
import in.ac.vit.andromedics.entities.Employee;
import in.ac.vit.andromedics.entities.Person;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.exceptions.ValidationException;
import in.ac.vit.andromedics.service.BranchService;
import in.ac.vit.andromedics.service.StoreService;
import in.ac.vit.andromedics.service.UserService;
import in.ac.vit.andromedics.utils.ParamParser;

import java.io.IOException;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

/**
 * Servlet implementation class EditStore
 */
@WebServlet("/users/edit.do")
@ServletSecurity(@HttpConstraint(rolesAllowed = UserRoles.ADMIN))
public class EditUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	private UserService userService;
	@Inject
	private StoreService storeService;
	@Inject
	private BranchService branchService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int userId = p.getInt("userId");
		Person person = this.userService.getById(userId);

		if (person == null || !(person instanceof Employee)) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		} else {
			Employee employee = (Employee) person;
			request.setAttribute("stores", this.storeService.getAll());
			request.setAttribute("branches",
					this.branchService.getByStore(employee.getStore().getId()));
			employee.setPassword("");
			request.setAttribute("employee", employee);
			request.getRequestDispatcher("/WEB-INF/views/users/form.jsp")
					.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int userId = p.getInt("id");
		if (userId == 0) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		Employee employee = (Employee) this.userService.getById(userId);
		employee.setAddress(p.getString("address"));
		int branchId = p.getInt("branch");
		Branch branch = this.branchService.getById(branchId);
		employee.setBranch(branch);
		if (branch != null) {
			employee.setStore(branch.getStore());
		} else {
			int storeId = p.getInt("store");
			employee.setStore(this.storeService.getById(storeId));
		}
		employee.setDateOfBirth(p.getDate("dateOfBirth"));
		employee.setEmail(p.getString("email"));
		employee.setFirstName(p.getString("firstName"));
		employee.setLastName(p.getString("lastName"));
		employee.setMiddleName(p.getString("middleName"));
		employee.setPassword(p.getString("password"));
		employee.setPhoneNo(p.getString("phoneNo"));
		String role = StringUtils.isEmpty(p.getString("role")) ? null
				: p.getString("role").equals(UserRoles.MANAGER) ? UserRoles.MANAGER
						: UserRoles.EMPLOYEE;
		employee.setRole(role);
		employee.setUsername(p.getString("username"));
		try {
			this.userService.update(employee);
			response.sendRedirect(getServletContext().getContextPath()
					+ "/users/view.do?userId="
					+ employee.getId());
		} catch (EJBException ex) {
			if (ex.getCause() instanceof ValidationException) {
				ValidationException ve = (ValidationException) ex.getCause();
				response.getWriter().print(ve.getAsJSON());
			} else {
				ex.printStackTrace();
			}
		}
	}

	protected StoreService getStoreService() {
		return this.storeService;
	}
}
