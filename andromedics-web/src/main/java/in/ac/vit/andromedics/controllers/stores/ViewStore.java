package in.ac.vit.andromedics.controllers.stores;

import in.ac.vit.andromedics.entities.Employee;
import in.ac.vit.andromedics.entities.Store;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.service.StoreService;
import in.ac.vit.andromedics.utils.ParamParser;
import org.apache.log4j.Logger;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class ViewStore
 */
@WebServlet("/stores/view.do")
public class ViewStore extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private transient Logger log = Logger.getLogger(getClass());
	@Inject
	private StoreService storeService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@RolesAllowed({ "admin", "manager" })
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int storeId = p.getInt("storeId");
		Store store = this.getStoreService().getById(storeId);
		if (store == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		} else {
			if (request.isUserInRole(UserRoles.MANAGER)) {
				Employee user = (Employee) request.getSession(false)
						.getAttribute("currentUser");
				if (user.getStore().getId() != store.getId()) {
					log.warn("User " + user.getId()
							+ " not allowed to view store " + storeId);
					response.sendError(HttpServletResponse.SC_FORBIDDEN);
					return;
				}
			}
			request.setAttribute("store", store);
			request.getRequestDispatcher("/WEB-INF/views/stores/view.jsp")
					.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

	protected StoreService getStoreService() {
		return this.storeService;
	}
}
