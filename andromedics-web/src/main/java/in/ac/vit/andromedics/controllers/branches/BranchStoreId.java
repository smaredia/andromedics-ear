package in.ac.vit.andromedics.controllers.branches;

import in.ac.vit.andromedics.entities.Branch;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.service.BranchService;
import in.ac.vit.andromedics.utils.ParamParser;
import org.codehaus.jackson.map.ObjectMapper;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: sahir
 * Date: 23/4/13
 * Time: 11:50 PM
 */
@WebServlet("/branches/getbystoreid")
@ServletSecurity(@HttpConstraint(rolesAllowed = {UserRoles.ADMIN, UserRoles.MANAGER}))
public class BranchStoreId extends HttpServlet {

    @Inject
    private BranchService branchService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ParamParser p = new ParamParser(request);
        int storeId = p.getInt("storeId");
        List<Branch> branches = this.branchService.getByStore(storeId);
        ArrayList<Map<String, Object>> branchList = new ArrayList<>(branches.size());
        for(Branch branch : branches) {
             Map<String, Object> map = new HashMap<>(2);
            map.put("id",branch.getId());
            map.put("name",branch.getName());
            branchList.add(map);
        }
        new ObjectMapper().writeValue(response.getWriter(), branchList);
    }
}
