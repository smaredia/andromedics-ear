package in.ac.vit.andromedics.controllers.users;

import in.ac.vit.andromedics.entities.Person;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.service.UserService;
import in.ac.vit.andromedics.utils.ParamParser;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

@WebServlet("/users/list.do")
@ServletSecurity(@HttpConstraint(rolesAllowed = { UserRoles.ADMIN }))
public class ListUser extends HttpServlet {

	@Inject
	private UserService userService;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		ParamParser p = new ParamParser(request);
		int sEcho = p.getInt("sEcho", -1);
		if (sEcho == -1) {
			request.getRequestDispatcher("/WEB-INF/views/users/list.jsp")
					.forward(request, response);
		} else {
			// echo json stores list
			int first = p.getInt("iDisplayStart");
			int pageSize = p.getInt("iDisplayLength");
			int sortCol = p.getInt("iSortCol_0");
			String sortField = "";
			switch (sortCol) {
			case 0:
				sortField = "id";
				break;
			case 1:
				sortField = "firstName";
				break;
			case 2:
				sortField = "lastName";
				break;
			case 3:
				sortField = "email";
				break;
			case 4:
				sortField = "role";
				break;
			default:
				sortField = "id";
			}
			String sortOrder = p.getString("sSortDir_0");
			Map<String, String> filters = new HashMap<>();

			String[] list = { "id", "firstName", "lastName", "email", "role" };
			for (int i = 0; i < list.length; i++) {
				String value = p.getString("sSearch_" + i, null);
				if (value != null) {
					filters.put(list[i], value);
				}
			}
			List<Person> users = this.userService.get(first, pageSize,
					sortField, sortOrder, filters);
			int totalCount = this.userService.getCount();
			int totalFilteredCount = this.userService.getCount(filters);
			Map<String, Object> map = new HashMap<>();
			map.put("sEcho", sEcho);
			map.put("iTotalRecords", totalCount);
			map.put("iTotalDisplayRecords", totalFilteredCount);
			String[][] aaData = new String[users.size()][];
			int i = 0;
			for (Person user : users) {
				String[] sData = new String[6];
				sData[0] = user.getId().toString();
				sData[1] = user.getFirstName();
				sData[2] = user.getLastName();
				sData[3] = user.getEmail();
				sData[4] = user.getRole();
				sData[5] = "<a href='javascript:;' onclick='loadForm(\"/users/view.do\",\"#content\",\"userId="
						+ user.getId() + "\")' class='viewLink'>view</a>";
				sData[5] += " <a href='javascript:;' onclick='loadForm(\"/users/edit.do\",\"#content\",\"userId="
						+ user.getId() + "\")' class='editLink'>edit</a>";
				sData[5] += " <a href='javascript:;' onclick='loadForm(\"/users/delete.do\",\"#content\",\"userId="
						+ user.getId() + "\")' class='deleteLink'>delete</a>";
				aaData[i++] = sData;
			}
			map.put("aaData", aaData);
			ObjectMapper mapper = new ObjectMapper();
			mapper.writeValue(response.getWriter(), map);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		this.doGet(request, response);
	}
}
