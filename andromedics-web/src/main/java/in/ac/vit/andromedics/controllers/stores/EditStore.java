package in.ac.vit.andromedics.controllers.stores;

import in.ac.vit.andromedics.entities.Status;
import in.ac.vit.andromedics.entities.Store;
import in.ac.vit.andromedics.exceptions.ValidationException;
import in.ac.vit.andromedics.service.StoreService;
import in.ac.vit.andromedics.utils.ParamParser;
import org.apache.log4j.Logger;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class EditStore
 */
@WebServlet("/stores/edit.do")
@RolesAllowed({ "admin" })
public class EditStore extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private transient Logger log = Logger.getLogger(getClass());
	@Inject
	private StoreService storeService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int storeId = p.getInt("storeId", -1);
		if (storeId == -1) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		} else {
			Store store = this.getStoreService().getById(storeId);
			if (store == null) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return;
			} else {
				request.setAttribute("store", store);
				request.getRequestDispatcher("/WEB-INF/views/stores/form.jsp")
						.forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int storeId = p.getInt("id");
		if (storeId != 0) {
			Store store = this.getStoreService().getById(storeId);
			store.setName(p.getString("name"));
			store.setStreet1(p.getString("street1"));
			store.setStreet2(p.getString("street2"));
			store.setCity(p.getString("city"));
			store.setState(p.getString("state"));
			store.setCountry(p.getString("country"));
			store.setPhoneNo(p.getString("phoneNo"));
			store.setPincode(p.getString("pincode"));
			store.setFax(p.getString("fax"));
			store.setWebsite(p.getString("website"));
			store.setEmail(p.getString("email"));
			store.setActive(p.getInt("active") == 1 ? Status.ACTIVE : Status.INACTIVE);
			store.setSubscriptionDate(p.getDate("subscriptionDate"));
			try {
				store = this.getStoreService().update(store);
				log.debug("redirecting...");
				response.sendRedirect(getServletContext().getContextPath()
						+ "/stores/view.do?storeId=" + store.getId());
			} catch (EJBException e) {
				if (e.getCause() instanceof ValidationException) {
					ValidationException ve = (ValidationException) e.getCause();
					response.getWriter().print(ve.getAsJSON());
					return;
				} else {
					e.printStackTrace();
				}
			}
		} else {
			// TODO add flash message here saying no store found
			response.sendRedirect(getServletContext().getContextPath()
					+ "/homepage.do?partial=true");
		}
	}

	protected StoreService getStoreService() {
		return this.storeService;
	}
}
