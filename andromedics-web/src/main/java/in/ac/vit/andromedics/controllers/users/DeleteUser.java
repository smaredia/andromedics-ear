package in.ac.vit.andromedics.controllers.users;

import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.service.UserService;
import in.ac.vit.andromedics.utils.ParamParser;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DeleteStore
 */
@WebServlet("/users/delete.do")
@ServletSecurity(@HttpConstraint(rolesAllowed=UserRoles.ADMIN))
public class DeleteUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Inject
	private UserService userService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int userId = p.getInt("userId");
		if (userId == 0) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		} else {
			this.userService.delete(userId);
			response.sendRedirect(getServletContext().getContextPath()
					+ "/users/list.do");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
