package in.ac.vit.andromedics.controllers.users;

import in.ac.vit.andromedics.entities.Person;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.service.UserService;
import in.ac.vit.andromedics.utils.ParamParser;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ViewStore
 */
@WebServlet("/users/view.do")
@ServletSecurity(@HttpConstraint(rolesAllowed = { UserRoles.ADMIN }))
public class ViewUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Inject
	private UserService userService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int userId = p.getInt("userId");
		Person user = this.userService.getById(userId);
		if (user == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		} else {
			request.setAttribute("user", user);
			request.getRequestDispatcher("/WEB-INF/views/users/view.jsp")
					.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
