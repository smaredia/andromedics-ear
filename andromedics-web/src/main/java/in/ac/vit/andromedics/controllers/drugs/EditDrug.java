package in.ac.vit.andromedics.controllers.drugs;

import in.ac.vit.andromedics.entities.Drug;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.exceptions.ValidationException;
import in.ac.vit.andromedics.service.DrugManufacturerService;
import in.ac.vit.andromedics.service.DrugService;
import in.ac.vit.andromedics.service.DrugTypeService;
import in.ac.vit.andromedics.utils.ParamParser;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class EditDrug
 */
@WebServlet("/drugs/edit.do")
@ServletSecurity(@HttpConstraint(rolesAllowed = { UserRoles.ADMIN }))
public class EditDrug extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Inject
	private DrugService drugService;
	@Inject
	private DrugManufacturerService drugManufacturerService;
	@Inject
	private DrugTypeService drugTypeService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int drugId = p.getInt("drugId");
		Drug drug = this.drugService.getById(drugId);
		if (drug == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		} else {
			request.setAttribute("manufacturers",
					this.drugManufacturerService.getAll());
			request.setAttribute("drugTypes", this.drugTypeService.getAll());
			request.setAttribute("drug", drug);
			request.getRequestDispatcher("/WEB-INF/views/drugs/form.jsp")
					.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int drugId = p.getInt("id");
		Drug drug = this.drugService.getById(drugId);
		if (drug == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		drug.setCode(p.getString("code"));
		drug.setConstituents(p.getString("constituents"));
		drug.setDiseases(p.getString("diseases"));
		drug.setDrugPower(p.getString("drugPower"));
		drug.setName(p.getString("name"));
		drug.setPrice(p.getDouble("price"));
		drug.setQuantity(p.getString("quantity"));
		int drugTypeId = p.getInt("drugType");
		drug.setDrugType(this.drugTypeService.getById(drugTypeId));
		int manufacturerId = p.getInt("drugManufacturer");
		drug.setDrugManufacturer(this.drugManufacturerService
				.getById(manufacturerId));
		try {
			drug = this.drugService.update(drug);
			response.sendRedirect(getServletContext().getContextPath()
					+ "/drugs/view.do?drugId=" + drug.getId());
		} catch (EJBException e) {
			if (e.getCause() instanceof ValidationException) {
				ValidationException ve = (ValidationException) e.getCause();
				response.getWriter().print(ve.getAsJSON());
				return;
			} else {
				e.printStackTrace();
			}
		}
	}

}
