package in.ac.vit.andromedics.controllers.stores;

import in.ac.vit.andromedics.entities.Store;
import in.ac.vit.andromedics.service.StoreService;
import in.ac.vit.andromedics.utils.ParamParser;
import in.ac.vit.andromedics.utils.Utils;
import org.codehaus.jackson.map.ObjectMapper;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet implementation class ListStores
 */
@WebServlet("/stores/list.do")
@RolesAllowed({ "admin" })
public class ListStores extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	private StoreService storeService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int sEcho = p.getInt("sEcho", -1);
		if (sEcho == -1) {
			request.getRequestDispatcher("/WEB-INF/views/stores/list.jsp")
					.forward(request, response);
		} else {
			// echo json stores list
			int first = p.getInt("iDisplayStart");
			int pageSize = p.getInt("iDisplayLength");
			int sortCol = p.getInt("iSortCol_0");
			String sortField = "";
			switch (sortCol) {
			case 0:
				sortField = "id";
				break;
			case 1:
				sortField = "name";
				break;
			case 2:
				sortField = "city";
				break;
			case 3:
				sortField = "state";
				break;
			case 4:
				sortField = "subscriptionDate";
				break;
			case 5:
				sortField = "active";
				break;
			default:
				sortField = "id";
			}
			String sortOrder = p.getString("sSortDir_0");
			Map<String, String> filters = new HashMap<>();

			String [] list = {"id","name", "city", "state", "subscriptionDate", "active"};
			for(int i=0; i<list.length; i++) {
				String value = p.getString("sSearch_"+i, null);
				if(value != null) {
					filters.put(list[i], value);
				}
			}
			List<Store> stores = this.getStoreService().get(first, pageSize,
					sortField, sortOrder, filters);
			int totalCount = this.getStoreService().getCount();
			int totalFilteredCount = this.getStoreService().getCount(filters);
			Map<String, Object> map = new HashMap<>();
			map.put("sEcho", sEcho);
			map.put("iTotalRecords", totalCount);
			map.put("iTotalDisplayRecords", totalFilteredCount);
			String[][] aaData = new String[stores.size()][];
			int i = 0;
			for (Store store : stores) {
				String[] sData = new String[7];
				sData[0] = store.getId().toString();
				sData[1] = store.getName();
				sData[2] = store.getCity();
				sData[3] = store.getState();
				sData[4] = Utils.dateToStr(store.getSubscriptionDate());
				sData[5] = store.getActive().toString();
				sData[6] = "<a href='javascript:;' onclick='loadForm(\"/stores/view.do\",\"#content\",\"storeId="+store.getId()+"\")' class='viewLink'>view</a>";
				sData[6] += " <a href='javascript:;' onclick='loadForm(\"/stores/edit.do\",\"#content\",\"storeId="+store.getId()+"\")' class='editLink'>edit</a>";
				sData[6] += " <a href='javascript:;' onclick='loadForm(\"/stores/delete.do\",\"#content\",\"storeId="+store.getId()+"\")' class='deleteLink'>delete</a>";
				aaData[i++] = sData;
			}
			map.put("aaData", aaData);
			ObjectMapper mapper = new ObjectMapper();
			mapper.writeValue(response.getWriter(), map);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

	protected StoreService getStoreService() {
		return this.storeService;
	}
}
