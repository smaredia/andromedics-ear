package in.ac.vit.andromedics.controllers;

import in.ac.vit.andromedics.entities.Person;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@SessionScoped
@Named("currentUser")
public class CurrentUser implements Serializable {

	private Person user;
	private String username;

	public Person getUser() {
		return user;
	}

	public void setUser(Person user) {
		this.user = user;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isLoggedIn() {
		return this.getUser() != null;
	}

	public boolean isUserRole(String role) {
		return this.getUser() == null ? false : this.getUser()
				.getRole().equals(role) ? true : false;
	}

	public String getName() {
		return this.getUser().getFirstName() + " "
				+ this.getUser().getLastName();
	}

}
