package in.ac.vit.andromedics.controllers.branches;

import in.ac.vit.andromedics.service.BranchService;
import in.ac.vit.andromedics.utils.ParamParser;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class DeleteBranch
 */
@WebServlet("/branches/delete.do")
public class DeleteBranch extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	private BranchService branchService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int branchId = p.getInt("branchId");
		this.branchService.delete(branchId);
		response.sendRedirect(getServletContext().getContextPath()
				+ "/branches/list.do");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
