package in.ac.vit.andromedics.controllers.branches;

import in.ac.vit.andromedics.controllers.CurrentUser;
import in.ac.vit.andromedics.entities.Branch;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.service.BranchService;
import in.ac.vit.andromedics.utils.ParamParser;
import org.codehaus.jackson.map.ObjectMapper;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet implementation class ListBranch
 */
@WebServlet("/branches/list.do")
@RolesAllowed({"admin", "manager"})
public class ListBranch extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	private BranchService branchService;
	@Inject
	private CurrentUser currentUser;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int sEcho = p.getInt("sEcho", -1);
		if (sEcho == -1) {
			request.getRequestDispatcher("/WEB-INF/views/branches/list.jsp")
					.forward(request, response);
		} else {
			// echo json stores list
			int first = p.getInt("iDisplayStart");
			int pageSize = p.getInt("iDisplayLength");
			int sortCol = p.getInt("iSortCol_0");
			String sortField = "";
			switch (sortCol) {
			case 0:
				sortField = "id";
				break;
			case 1:
				sortField = "name";
				break;
			case 2:
				sortField = "city";
				break;
			case 3:
				sortField = "state";
				break;
			case 4:
				sortField = "active";
				break;
			case 5:
				sortField = "store";
				break;
			default:
				sortField = "id";
			}
			String sortOrder = p.getString("sSortDir_0");
			Map<String, String> filters = new HashMap<>();

			ArrayList<String> list = new ArrayList<>();
			list.add("id");
			list.add("name");
			list.add("city");
			list.add("state");
			list.add("active");

			if (this.currentUser.isUserRole(UserRoles.ADMIN)) {
				list.add("store");
			}
			for (int i = 0; i < list.size(); i++) {
				String value = p.getString("sSearch_" + i, null);
				if (value != null) {
					filters.put(list.get(i), value);
				}
			}
			List<Branch> branches = this.branchService.get(first, pageSize,
					sortField, sortOrder, filters);
			int totalCount = this.branchService.getCount();
			int totalFilteredCount = this.branchService.getCount(filters);
			Map<String, Object> map = new HashMap<>();
			map.put("sEcho", sEcho);
			map.put("iTotalRecords", totalCount);
			map.put("iTotalDisplayRecords", totalFilteredCount);
			ArrayList<ArrayList<String>> aaData = new ArrayList<>();
			for (Branch branch : branches) {
				ArrayList<String> sData = new ArrayList<>();
				sData.add(branch.getId().toString());
				sData.add(branch.getName());
				sData.add(branch.getCity());
				sData.add(branch.getState());
				sData.add(branch.getActive().toString());
				if (this.currentUser.isUserRole(UserRoles.ADMIN)) {
					sData.add(branch.getStore().getName());
				}

				String link = "<a href='javascript:;' onclick='loadForm(\"/branches/view.do\",\"#content\",\"branchId="
						+ branch.getId() + "\")' class='viewLink'>view</a>";
				link += " <a href='javascript:;' onclick='loadForm(\"/branches/edit.do\",\"#content\",\"branchId="
						+ branch.getId() + "\")' class='editLink'>edit</a>";
				link += " <a href='javascript:;' onclick='loadForm(\"/branches/delete.do\",\"#content\",\"branchId="
						+ branch.getId() + "\")' class='deleteLink'>delete</a>";
				sData.add(link);
				aaData.add(sData);
			}
			map.put("aaData", aaData);
			ObjectMapper mapper = new ObjectMapper();
			mapper.writeValue(response.getWriter(), map);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
