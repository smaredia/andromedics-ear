package in.ac.vit.andromedics.controllers.manufacturers;

import in.ac.vit.andromedics.service.DrugManufacturerService;
import in.ac.vit.andromedics.utils.ParamParser;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class DeleteManufacturer
 */
@WebServlet("/manufacturers/delete.do")
public class DeleteManufacturer extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	private DrugManufacturerService drugManufacturerService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int manufacturerId = p.getInt("manufacturerId");
		this.drugManufacturerService.delete(manufacturerId);
		response.sendRedirect(getServletContext().getContextPath()
				+ "/manufacturers/list.do");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
