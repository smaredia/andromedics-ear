package in.ac.vit.andromedics.controllers;


import org.jboss.security.auth.spi.Util;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/encrypt")
public class EncryptedPassword extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().print(Util.createPasswordHash("MD5", "hex", null, null, "sahir123"));
    }
}