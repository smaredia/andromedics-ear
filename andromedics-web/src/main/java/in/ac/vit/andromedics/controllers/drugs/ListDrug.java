package in.ac.vit.andromedics.controllers.drugs;

import in.ac.vit.andromedics.entities.Drug;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.service.DrugService;
import in.ac.vit.andromedics.utils.ParamParser;
import org.codehaus.jackson.map.ObjectMapper;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet implementation class ListDrug
 */
@WebServlet("/drugs/list.do")
@ServletSecurity(@HttpConstraint(rolesAllowed = { UserRoles.ADMIN,
		UserRoles.CUSTOMER, UserRoles.EMPLOYEE, UserRoles.MANAGER }))
public class ListDrug extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Inject
	private DrugService drugService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ParamParser p = new ParamParser(request);
		int sEcho = p.getInt("sEcho", -1);
		if (sEcho == -1) {
			request.getRequestDispatcher("/WEB-INF/views/drugs/list.jsp")
					.forward(request, response);
		} else {
			// echo json stores list
			int first = p.getInt("iDisplayStart");
			int pageSize = p.getInt("iDisplayLength");
			int sortCol = p.getInt("iSortCol_0");
			String sortField = "";
			switch (sortCol) {
			case 0:
				sortField = "id";
				break;
			case 1:
				sortField = "name";
				break;
			case 2:
				sortField = "code";
				break;
			case 3:
				sortField = "drugManufacturer";
				break;
			default:
				sortField = "id";
			}
			String sortOrder = p.getString("sSortDir_0");
			Map<String, String> filters = new HashMap<>();

			ArrayList<String> list = new ArrayList<>();
			list.add("id");
			list.add("name");
			list.add("code");
			list.add("drugManufacturer");
			for (int i = 0; i < list.size(); i++) {
				String value = p.getString("sSearch_" + i, null);
				if (value != null) {
					filters.put(list.get(i), value);
				}
			}
			List<Drug> drugs = this.drugService.get(first, pageSize, sortField,
					sortOrder, filters);
			int totalCount = this.drugService.getCount();
			int totalFilteredCount = this.drugService.getCount(filters);
			Map<String, Object> map = new HashMap<>();
			map.put("sEcho", sEcho);
			map.put("iTotalRecords", totalCount);
			map.put("iTotalDisplayRecords", totalFilteredCount);
			ArrayList<ArrayList<String>> aaData = new ArrayList<>();
			for (Drug drug : drugs) {
				ArrayList<String> sData = new ArrayList<>();
				sData.add(drug.getId().toString());
				sData.add(drug.getName());
				sData.add(drug.getCode());
				sData.add(drug.getDrugManufacturer().getName());

				String link = "<a href='javascript:;' onclick='loadForm(\"/drugs/view.do\",\"#content\",\"drugId="
						+ drug.getId() + "\")' class='viewLink'>view</a>";
				link += " <a href='javascript:;' onclick='loadForm(\"/drugs/edit.do\",\"#content\",\"drugId="
						+ drug.getId() + "\")' class='editLink'>edit</a>";
				link += " <a href='javascript:;' onclick='loadForm(\"/drugs/delete.do\",\"#content\",\"drugId="
						+ drug.getId() + "\")' class='deleteLink'>delete</a>";
				sData.add(link);
				aaData.add(sData);
			}
			map.put("aaData", aaData);
			ObjectMapper mapper = new ObjectMapper();
			mapper.writeValue(response.getWriter(), map);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
