package in.ac.vit.andromedics.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-04-04T18:54:36.990+0530")
@StaticMetamodel(DrugType.class)
public class DrugType_ {
	public static volatile SingularAttribute<DrugType, Integer> id;
	public static volatile SingularAttribute<DrugType, String> name;
}
