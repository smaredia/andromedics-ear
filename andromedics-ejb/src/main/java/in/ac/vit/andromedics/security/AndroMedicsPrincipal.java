package in.ac.vit.andromedics.security;

import in.ac.vit.andromedics.entities.Person;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.service.UserService;
import org.apache.log4j.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.security.Principal;

public class AndroMedicsPrincipal implements Principal {
	private String name;
	private Person person;
	private UserService userService;
	private String fullname;
	private transient Logger log = Logger.getLogger(getClass());

	public AndroMedicsPrincipal(String name) {
		this.name = name;
		InitialContext ic;
		if (!(name.equals(UserRoles.ADMIN) || name.equals(UserRoles.CUSTOMER)
				|| name.equals(UserRoles.EMPLOYEE) || name
					.equals(UserRoles.MANAGER))) {
			try {
				ic = new InitialContext();
				userService = (UserService) ic
						.lookup("java:app/andromedics-ejb/UserServiceImpl");

				this.person = userService.getByUsername(name);
				this.fullname = this.person.fullName();
			} catch (NamingException e) {
				log.error("Cannot lookup UserServiceImpl", e);
			}
		}
	}

	@Override
	public String getName() {
		return name;
	}

	public Person getUser() {
		return this.person;
	}

	public String getFullName() {
		return this.fullname;
	}
}