package in.ac.vit.andromedics.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-04-04T18:54:37.027+0530")
@StaticMetamodel(StockBlockProduct.class)
public class StockBlockProduct_ {
	public static volatile SingularAttribute<StockBlockProduct, Integer> id;
	public static volatile SingularAttribute<StockBlockProduct, Integer> quantity;
	public static volatile SingularAttribute<StockBlockProduct, Drug> drug;
	public static volatile SingularAttribute<StockBlockProduct, StockBlock> stockBlock;
}
