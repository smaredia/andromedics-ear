package in.ac.vit.andromedics.entities;

public class UserRoles {
	public final static String ANONYMOUS = "anonymous";
	public final static String ADMIN = "admin";
	public final static String MANAGER = "manager";
	public final static String EMPLOYEE = "employee";
	public final static String CUSTOMER = "customer";
}
