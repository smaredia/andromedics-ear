package in.ac.vit.andromedics.service;

import in.ac.vit.andromedics.entities.Drug;

import java.util.List;

public interface DrugService extends AbstractService<Drug> {
	List<Drug> getAll();

	Drug getByName(String name);

	Drug getByCode(String code);

	List<Drug> getByManufacturer(int id);

	List<Drug> getByManufacturerAndType(int manufacturerId, int typeId);

	List<Drug> getNameLike(String name);
}
