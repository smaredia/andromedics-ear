package in.ac.vit.andromedics.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-04-04T18:54:36.980+0530")
@StaticMetamodel(DrugManufacturer.class)
public class DrugManufacturer_ {
	public static volatile SingularAttribute<DrugManufacturer, Integer> id;
	public static volatile SingularAttribute<DrugManufacturer, String> name;
}
