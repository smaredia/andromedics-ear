package in.ac.vit.andromedics.dao;

import in.ac.vit.andromedics.entities.Stock;

public interface StockDao extends AbstractDao<Stock> {
	Stock getByDrug(int drugId, int storeId, int branchId);
}
