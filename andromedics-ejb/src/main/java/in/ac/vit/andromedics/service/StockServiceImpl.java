package in.ac.vit.andromedics.service;

import in.ac.vit.andromedics.dao.StockDao;
import in.ac.vit.andromedics.entities.Branch;
import in.ac.vit.andromedics.entities.Drug;
import in.ac.vit.andromedics.entities.Employee;
import in.ac.vit.andromedics.entities.Stock;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.entities.form.DrugBean;
import in.ac.vit.andromedics.exceptions.ValidationException;
import in.ac.vit.andromedics.security.AndroMedicsPrincipal;

import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

@Stateless
public class StockServiceImpl implements StockService {

	@Inject
	private StockDao stockDao;
	@Inject
	private Validator validator;
	@Inject
	private DrugService drugService;
	@Inject
	private BranchService branchService;
	@Resource
	private SessionContext context;

	@Override
	public Stock save(Stock stock) {
		Set<ConstraintViolation<Stock>> violations = this.validator
				.validate(stock);
		if (!violations.isEmpty()) {
			throw new ValidationException(new HashSet<ConstraintViolation<?>>(
					violations));
		}
		stock = this.stockDao.save(stock);
		return stock;
	}

	@Override
	public Stock update(Stock stock) {
		Set<ConstraintViolation<Stock>> violations = this.validator
				.validate(stock);
		if (!violations.isEmpty()) {
			throw new ValidationException(new HashSet<ConstraintViolation<?>>(
					violations));
		}
		stock = this.stockDao.update(stock);
		return stock;
	}

	@Override
	public void delete(Stock stock) {
		if (stock == null) {
			return;
		} else {
			this.stockDao.delete(stock);
		}
	}

	@Override
	public void delete(int id) {
		if (id <= 0) {
			return;
		} else {
			this.stockDao.deleteById(id);
		}

	}

	@Override
	public int getCount() {
		return this.stockDao.getCount().intValue();
	}

	@Override
	public Stock getById(int id) {
		if (id <= 0) {
			return null;
		} else {
			return this.stockDao.getById(id);
		}
	}

	@Override
	public List<Stock> get(int first, int pageSize, String sortField,
			String sortOrder, Map<String, String> filters) {
		return this.stockDao
				.get(first, pageSize, sortField, sortOrder, filters);
	}

	@Override
	public int getCount(Map<String, String> filters) {
		return this.getCount(filters);
	}

	@Override
	@RolesAllowed({ UserRoles.MANAGER, UserRoles.EMPLOYEE })
	public void addStock(List<DrugBean> drugs) {
		Principal p = context.getCallerPrincipal();
		AndroMedicsPrincipal ap = (AndroMedicsPrincipal) p;
		Employee e = (Employee) ap.getUser();
		int storeId = e.getStore().getId();
		int branchId = e.getBranch().getId();
		Branch branch = null;
		ValidationException ve = new ValidationException(
				new HashSet<ConstraintViolation<?>>());
		for (DrugBean db : drugs) {
			Drug drug = this.drugService.getById(db.getDrugId());
			if (drug == null) {
				ve.addError("drug", "Drug with id = " + db.getDrugId()
						+ " not found");
				continue;
			}
			if (db.getQty() <= 0) {
				ve.addError("Qty drugId " + db.getDrugId(),
						"Invalid Qty provided for " + drug.getName());
			}

			// Run only if till now there are no errors
			if (ve.isEmpty()) {
				if (e.getRole().equals(UserRoles.MANAGER)) {
					branchId = db.getBranchId();
				}
				Stock stock = this.stockDao.getByDrug(drug.getId(), storeId,
						branchId);
				if (stock == null) {
					if (branch == null || branch.getId() != branchId) {
						branch = this.branchService.getById(branchId);
						if (branch == null
								|| branch.getStore().getId() != storeId) {
							ve.addError("Branch drugId = " + db.getDrugId(),
									"Branch with id = " + db.getBranchId()
											+ " not found");
							continue;
						}
					}
					stock = new Stock();
					stock.setBranch(branch);
					stock.setQuantity((db.getQty() / 2));
					stock.setDrug(drug);
					stock.setReorderLevel(db.getQty());
					this.stockDao.save(stock);
				} else {
					stock.setQuantity(stock.getQuantity() + db.getQty());
					this.stockDao.update(stock);
				}
			}

		}

		if (!ve.isEmpty()) {
			throw ve;
		}

	}

	@Override
	@RolesAllowed({ UserRoles.EMPLOYEE, UserRoles.MANAGER })
	public Stock getByDrug(int drugId, int storeId, int branchId) {
		if (drugId <= 0 || storeId <= 0 || branchId <= 0) {
			return null;
		} else {
			Principal p = context.getCallerPrincipal();
			AndroMedicsPrincipal ap = (AndroMedicsPrincipal) p;
			Employee e = (Employee) ap.getUser();
			storeId = e.getStore().getId();
			branchId = e.getBranch().getId();
			if (e.getRole().equals(UserRoles.MANAGER)) {
				Branch branch = this.branchService.getById(branchId);
				if (branch == null || branch.getStore().getId() != storeId) {
					return null;
				} else {
					branchId = branch.getId();
				}
			}
			return this.stockDao.getByDrug(drugId, storeId, branchId);
		}
	}

}
