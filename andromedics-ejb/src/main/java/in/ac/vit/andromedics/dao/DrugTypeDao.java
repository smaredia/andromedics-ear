package in.ac.vit.andromedics.dao;

import in.ac.vit.andromedics.entities.DrugType;

import java.util.List;

public interface DrugTypeDao extends AbstractDao<DrugType> {
	List<DrugType> getAll();
}
