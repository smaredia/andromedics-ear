package in.ac.vit.andromedics.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


/**
 * The persistent class for the drug_type database table.
 */
@Entity
@XmlRootElement
@Table(name = "drug_type")
public class DrugType implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String name;

    public DrugType() {
    }


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    @Column(nullable = false, length = 200)
    @NotNull
    @Size(max=3)
    public String getName() {
        return this.name;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

}