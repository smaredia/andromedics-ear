package in.ac.vit.andromedics.service;

import in.ac.vit.andromedics.dao.DrugDao;
import in.ac.vit.andromedics.entities.Drug;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.exceptions.ValidationException;
import org.apache.commons.lang.StringUtils;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Stateless
public class DrugServiceImpl implements DrugService {

	@Inject
	private DrugDao drugDao;
	@Inject
	private Validator validator;

	@Override
	@RolesAllowed({ UserRoles.ADMIN })
	public Drug save(Drug drug) {
		Set<ConstraintViolation<Drug>> violations = this.validator
				.validate(drug);
		ValidationException exp = new ValidationException(
				new HashSet<ConstraintViolation<?>>(violations));

		if (!StringUtils.isEmpty(drug.getName())) {
			Drug oldDrug = this.drugDao.getByName(drug.getName());
			if (oldDrug != null) {
				exp.addError("name", "drug already exists");
			}
		}
		if (!StringUtils.isEmpty(drug.getCode())) {
			Drug oldDrug = this.drugDao.getByCode(drug.getCode());
			if (oldDrug != null) {
				exp.addError("code", "code already taken");
			}
		}
		if (!exp.isEmpty()) {
			throw exp;
		}
		drug = this.drugDao.save(drug);
		return drug;
	}

	@Override
	@RolesAllowed({ UserRoles.ADMIN })
	public Drug update(Drug drug) {
		Set<ConstraintViolation<Drug>> violations = this.validator
				.validate(drug);
		ValidationException exp = new ValidationException(
				new HashSet<ConstraintViolation<?>>(violations));
		if (!exp.isEmpty()) {
			throw exp;
		}
		drug = this.drugDao.update(drug);
		return drug;
	}

	@Override
	@RolesAllowed({ UserRoles.ADMIN })
	public void delete(Drug drug) {
		if (drug != null) {
			this.drugDao.delete(drug);
		}
	}

	@Override
	@RolesAllowed({ UserRoles.ADMIN })
	public void delete(int id) {
		if (id > 0) {
			this.drugDao.deleteById(id);
		}
	}

	@Override
	public int getCount() {
		return this.drugDao.getCount().intValue();
	}

	@Override
	public Drug getById(int id) {
		if (id > 0) {
			return this.drugDao.getById(id);
		} else {
			return null;
		}
	}

	@Override
	public List<Drug> get(int first, int pageSize, String sortField,
			String sortOrder, Map<String, String> filters) {
		return this.drugDao.get(first, pageSize, sortField, sortOrder, filters);
	}

	@Override
	public int getCount(Map<String, String> filters) {
		return this.drugDao.getCount(filters);
	}

	@Override
	public List<Drug> getAll() {
		return this.drugDao.getAll();
	}

	@Override
	public Drug getByName(String name) {
		if (!StringUtils.isEmpty(name)) {
			return this.drugDao.getByName(name);
		} else {
			return null;
		}
	}

	@Override
	public Drug getByCode(String code) {
		if (!StringUtils.isEmpty(code)) {
			return this.drugDao.getByCode(code);
		} else {
			return null;
		}
	}

	@Override
	public List<Drug> getByManufacturer(int id) {
		if (id <= 0) {
			return null;
		} else {
			return this.drugDao.getByManufacturer(id);
		}
	}

	@Override
	public List<Drug> getByManufacturerAndType(int manufacturerId, int typeId) {
		if (manufacturerId <= 0 || typeId <= 0) {
			return null;
		} else {
			return this.drugDao
					.getByManufacturerAndType(manufacturerId, typeId);
		}
	}

	@Override
	public List<Drug> getNameLike(String name) {
		if (StringUtils.isEmpty(name)) {
			return new ArrayList<Drug>();
		} else {
			return this.drugDao.getNameLike(name);
		}
	}

}