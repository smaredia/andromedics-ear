package in.ac.vit.andromedics.service;

import in.ac.vit.andromedics.entities.DrugType;

import java.util.List;

public interface DrugTypeService extends AbstractService<DrugType> {
	List<DrugType> getAll();
}
