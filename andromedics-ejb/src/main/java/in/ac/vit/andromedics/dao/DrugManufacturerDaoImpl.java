package in.ac.vit.andromedics.dao;

import in.ac.vit.andromedics.entities.DrugManufacturer;
import in.ac.vit.andromedics.entities.DrugManufacturer_;
import org.apache.commons.lang.StringUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Stateless
public class DrugManufacturerDaoImpl extends AbstractDaoImpl<DrugManufacturer>
		implements DrugManufacturerDao {

	@Inject
	private EntityManager entityManager;

	@Override
	public List<DrugManufacturer> get(int first, int pageSize,
			String sortField, String sortOrder, Map<String, String> filters) {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<DrugManufacturer> criteria = builder
				.createQuery(DrugManufacturer.class);
		Root<DrugManufacturer> drugManufacturer = criteria
				.from(DrugManufacturer.class);

		this.buildFilters(filters, criteria, builder, drugManufacturer);

		if (!StringUtils.isBlank(sortField)) {
			Path<?> path = null;
			switch (sortField) {
			case "id":
				path = drugManufacturer.get(DrugManufacturer_.id);
				break;
			case "name":
				path = drugManufacturer.get(DrugManufacturer_.name);
				break;
			}

			if (sortOrder.equalsIgnoreCase("asc")) {
				criteria.orderBy(builder.asc(path));
			} else {
				criteria.orderBy(builder.desc(path));
			}

		}
		TypedQuery<DrugManufacturer> query = this.getEntityManager()
				.createQuery(criteria);
		return query.setFirstResult(first).setMaxResults(pageSize)
				.getResultList();
	}

	@Override
	public int getCount(Map<String, String> filters) {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<DrugManufacturer> drugManufacturer = criteria
				.from(DrugManufacturer.class);
		criteria.select(builder.count(drugManufacturer));

		this.buildFilters(filters, criteria, builder, drugManufacturer);
		TypedQuery<Long> query = this.getEntityManager().createQuery(criteria);
		return query.getSingleResult().intValue();
	}

	private void buildFilters(Map<String, String> filters,
			CriteriaQuery<?> criteria, CriteriaBuilder builder,
			Root<DrugManufacturer> drugManufacturer) {

		if (filters == null)
			return;

		Set<String> keys = filters.keySet();
		ArrayList<Predicate> predicates = new ArrayList<>();
		for (String key : keys) {
			String value = filters.get(key);
			switch (key) {
			case "id":
				predicates.add(builder.equal(
						drugManufacturer.get(DrugManufacturer_.id), value));
				break;
			case "name":
				predicates.add(builder.like(
						drugManufacturer.get(DrugManufacturer_.name), value
								+ "%"));
				break;
			}
		}
		Predicate[] predicateArr = new Predicate[predicates.size()];
		for (int i = 0; i < predicateArr.length; i++) {
			predicateArr[i] = predicates.get(i);
		}
		criteria.where(predicateArr);
	}

	@Override
	public List<DrugManufacturer> getAll() {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<DrugManufacturer> criteria = builder
				.createQuery(DrugManufacturer.class);
		Root<DrugManufacturer> drugManufacturer = criteria
				.from(DrugManufacturer.class);
		criteria.orderBy(builder.asc(drugManufacturer
				.get(DrugManufacturer_.name)));

		TypedQuery<DrugManufacturer> query = this.getEntityManager()
				.createQuery(criteria);
		return query.getResultList();
	}

	@Override
	protected EntityManager getEntityManager() {
		return this.entityManager;
	}

}
