package in.ac.vit.andromedics.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * The persistent class for the drug_manufacturer database table.
 */
@Entity
@XmlRootElement
@Table(name = "drug_manufacturer")
public class DrugManufacturer implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;

	public DrugManufacturer() {
	}

	@Id
	@SequenceGenerator(name = "drugManufacturerSeq", sequenceName = "drug_manufacturer_id_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drugManufacturerSeq")
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min = 3)
	public String getName() {
		return this.name;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

}