package in.ac.vit.andromedics.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-04-04T18:54:36.992+0530")
@StaticMetamodel(Employee.class)
public class Employee_ extends Person_ {
	public static volatile SingularAttribute<Employee, Branch> branch;
	public static volatile SingularAttribute<Employee, Store> store;
}
