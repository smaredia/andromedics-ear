package in.ac.vit.andromedics.service;

import in.ac.vit.andromedics.dao.DrugTypeDao;
import in.ac.vit.andromedics.entities.DrugType;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

@Stateless
public class DrugTypeServiceImpl implements DrugTypeService {

	@Inject
	private DrugTypeDao drugTypeDao;

	@Override
	public DrugType save(DrugType x) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DrugType update(DrugType x) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(DrugType x) {
		// TODO Auto-generated method stub
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public DrugType getById(int id) {
		if(id > 0 ) {
			return this.drugTypeDao.getById(id);
		} else {
			return null;
		}
	}

	@Override
	public List<DrugType> get(int first, int pageSize, String sortField,
			String sortOrder, Map<String, String> filters) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getCount(Map<String, String> filters) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<DrugType> getAll() {
		return this.drugTypeDao.getAll();
	}

}
