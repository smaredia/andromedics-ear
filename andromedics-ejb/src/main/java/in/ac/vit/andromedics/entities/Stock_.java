package in.ac.vit.andromedics.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-04-04T18:54:37.010+0530")
@StaticMetamodel(Stock.class)
public class Stock_ {
	public static volatile SingularAttribute<Stock, Integer> id;
	public static volatile SingularAttribute<Stock, Integer> quantity;
	public static volatile SingularAttribute<Stock, Integer> reorderLevel;
	public static volatile SingularAttribute<Stock, Branch> branch;
	public static volatile SingularAttribute<Stock, Drug> drug;
}
