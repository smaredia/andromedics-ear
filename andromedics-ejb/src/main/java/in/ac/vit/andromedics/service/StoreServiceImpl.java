package in.ac.vit.andromedics.service;

import in.ac.vit.andromedics.dao.StoreDao;
import in.ac.vit.andromedics.entities.Store;
import in.ac.vit.andromedics.exceptions.ValidationException;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA. User: sahir Date: 1/4/13 Time: 12:14 AM To change
 * this template use File | Settings | File Templates.
 */
@Stateless
public class StoreServiceImpl implements StoreService {

	@Inject
	private StoreDao storeDao;
	@Inject
	private Validator validator;

	@Override
	public Store getById(int id) {
		return this.getStoreDao().getById(id);
	}

	@Override
	public List<Store> get(int first, int pageSize, String sortField,
			String sortOrder, Map<String, String> filters) {
		return this.getStoreDao().get(first, pageSize, sortField, sortOrder,
				filters);
	}

	@Override
	public int getCount() {
		return this.getStoreDao().getCount().intValue();
	}

	@Override
	@RolesAllowed("admin")
	public Store save(Store store) {
		Set<ConstraintViolation<Store>> violations = this.getValidator()
				.validate(store);
		if (!violations.isEmpty()) {
			throw new ValidationException(new HashSet<ConstraintViolation<?>>(
					violations));
		}
		store = this.getStoreDao().save(store);
		return store;
	}

	@Override
	@RolesAllowed("admin")
	public Store update(Store store) {
		Set<ConstraintViolation<Store>> violations = this.getValidator()
				.validate(store);
		if (!violations.isEmpty()) {
			throw new ValidationException(new HashSet<ConstraintViolation<?>>(
					violations));
		}
		return this.getStoreDao().update(store);
	}

	@Override
	public void delete(Store store) {
		this.getStoreDao().delete(store);
	}

	protected StoreDao getStoreDao() {
		return this.storeDao;
	}

	protected Validator getValidator() {
		return this.validator;
	}

	@Override
	public void delete(int id) {
		Store s = this.getById(id);
		if (s != null) {
			this.delete(s);
		}
	}

	@Override
	public int getCount(Map<String, String> filters) {
		return this.getStoreDao().getCount(filters);
	}

	@Override
	public List<Store> getAll() {
		return this.getStoreDao().getAll();
	}
}
