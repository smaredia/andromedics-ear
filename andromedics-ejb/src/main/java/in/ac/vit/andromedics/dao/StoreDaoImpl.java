/**
 *
 */
package in.ac.vit.andromedics.dao;

import in.ac.vit.andromedics.entities.Status;
import in.ac.vit.andromedics.entities.Store;
import in.ac.vit.andromedics.entities.Store_;
import in.ac.vit.andromedics.utils.Utils;
import org.apache.commons.lang.StringUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;

/**
 * @author sahir
 */
@Stateless
public class StoreDaoImpl extends AbstractDaoImpl<Store> implements StoreDao {

	@Inject
	EntityManager entityManager;

	@Override
	public List<Store> getAll() {
		List<Store> stores = this.getEntityManager()
				.createNamedQuery("Store.findAll", Store.class).getResultList();
		return stores;
	}

	@Override
	protected EntityManager getEntityManager() {
		return this.entityManager;
	}

	@Override
	public List<Store> get(int first, int pageSize, String sortField,
			String sortOrder, Map<String, String> filters) {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Store> criteria = builder.createQuery(Store.class);
		Root<Store> store = criteria.from(Store.class);

		this.buildFilters(filters, criteria, builder, store);

		if (!StringUtils.isBlank(sortField)) {
			Path<?> path = null;
			switch (sortField) {
			case "id":
				path = store.get(Store_.id);
				break;
			case "name":
				path = store.get(Store_.name);
				break;
			case "city":
				path = store.get(Store_.city);
				break;
			case "state":
				path = store.get(Store_.state);
				break;
			case "subscriptionDate":
				path = store.get(Store_.subscriptionDate);
				break;
			case "active":
				path = store.get(Store_.active);
			}

			if (sortOrder.equalsIgnoreCase("asc")) {
				criteria.orderBy(builder.asc(path));
			} else {
				criteria.orderBy(builder.desc(path));
			}

		}
		TypedQuery<Store> query = this.getEntityManager().createQuery(criteria);
		return query.setFirstResult(first).setMaxResults(pageSize)
				.getResultList();
	}

	private void buildFilters(Map<String, String> filters,
			CriteriaQuery<?> criteria, CriteriaBuilder builder,
			Root<Store> store) {

		if (filters == null)
			return;

		Set<String> keys = filters.keySet();
		ArrayList<Predicate> predicates = new ArrayList<>();
		for (String key : keys) {
			String value = filters.get(key);
			switch (key) {
			case "id":
				predicates.add(builder.equal(store.get(Store_.id), value));
				break;
			case "name":
				predicates
						.add(builder.like(store.get(Store_.name), value + "%"));
				break;
			case "city":
				predicates
						.add(builder.like(store.get(Store_.city), value + "%"));
				break;
			case "state":
				predicates.add(builder.like(store.get(Store_.state), value
						+ "%"));
				break;
			case "subscriptionDate":
				Date date = Utils.strToDate(value);
				if (date != null)
					predicates.add(builder.equal(
							store.get(Store_.subscriptionDate), date));
				break;
			case "active":
				predicates.add(builder.equal(store.get(Store_.active),
						Status.valueOf(value)));
				break;
			}
		}
		Predicate[] predicateArr = new Predicate[predicates.size()];
		for (int i = 0; i < predicateArr.length; i++) {
			predicateArr[i] = predicates.get(i);
		}
		criteria.where(predicateArr);
	}

	@Override
	public int getCount(Map<String, String> filters) {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Store> store = criteria.from(Store.class);
		criteria.select(builder.count(store));

		this.buildFilters(filters, criteria, builder, store);
		TypedQuery<Long> query = this.getEntityManager().createQuery(criteria);
		return query.getSingleResult().intValue();
	}

}
