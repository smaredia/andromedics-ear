package in.ac.vit.andromedics.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-04-04T18:54:36.978+0530")
@StaticMetamodel(Drug.class)
public class Drug_ {
	public static volatile SingularAttribute<Drug, String> code;
	public static volatile SingularAttribute<Drug, String> constituents;
	public static volatile SingularAttribute<Drug, String> diseases;
	public static volatile SingularAttribute<Drug, DrugManufacturer> drugManufacturer;
	public static volatile SingularAttribute<Drug, String> drugPower;
	public static volatile SingularAttribute<Drug, DrugType> drugType;
	public static volatile SingularAttribute<Drug, Integer> id;
	public static volatile SingularAttribute<Drug, String> name;
	public static volatile SingularAttribute<Drug, Double> price;
	public static volatile SingularAttribute<Drug, String> quantity;
}
