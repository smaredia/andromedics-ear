package in.ac.vit.andromedics.service;

import in.ac.vit.andromedics.dao.DrugManufacturerDao;
import in.ac.vit.andromedics.entities.DrugManufacturer;
import in.ac.vit.andromedics.exceptions.ValidationException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Stateless
public class DrugManufacturerServiceImpl implements
		DrugManufacturerService {

	@Inject
	private Validator validator;

	@Override
	public DrugManufacturer save(DrugManufacturer drugManufacturer) {
		Set<ConstraintViolation<DrugManufacturer>> violations = this.validator
				.validate(drugManufacturer);
		if (!violations.isEmpty()) {
			throw new ValidationException(new HashSet<ConstraintViolation<?>>(
					violations));
		}
		drugManufacturer = this.drugManufacturerDao.save(drugManufacturer);
		return drugManufacturer;
	}

	@Override
	public DrugManufacturer update(DrugManufacturer drugManufacturer) {
		Set<ConstraintViolation<DrugManufacturer>> violations = this.validator
				.validate(drugManufacturer);
		if (!violations.isEmpty()) {
			throw new ValidationException(new HashSet<ConstraintViolation<?>>(
					violations));
		}
		drugManufacturer = this.drugManufacturerDao.update(drugManufacturer);
		return drugManufacturer;
	}

	@Override
	public void delete(DrugManufacturer drugManufacturer) {
		if (drugManufacturer != null) {
			this.drugManufacturerDao.delete(drugManufacturer);
		}
	}

	@Override
	public void delete(int id) {
		if (id > 0) {
			this.drugManufacturerDao.deleteById(id);
		}
	}

	@Override
	public int getCount() {
		return this.drugManufacturerDao.getCount().intValue();
	}

	@Override
	public DrugManufacturer getById(int id) {
		if (id > 0) {
			return this.drugManufacturerDao.getById(id);
		} else {
			return null;
		}
	}

	@Override
	public List<DrugManufacturer> get(int first, int pageSize,
			String sortField, String sortOrder, Map<String, String> filters) {
		return this.drugManufacturerDao.get(first, pageSize, sortField,
				sortOrder, filters);
	}

	@Override
	public int getCount(Map<String, String> filters) {
		return this.drugManufacturerDao.getCount(filters);
	}

	@Inject
	private DrugManufacturerDao drugManufacturerDao;

	@Override
	public List<DrugManufacturer> getAll() {
		return this.drugManufacturerDao.getAll();
	}

}
