package in.ac.vit.andromedics.entities.form;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DrugBean {

	private int drugId;
	private int qty;
	private int storeId;
	private int branchId;

	public int getDrugId() {
		return drugId;
	}

	public void setDrugId(int drugId) {
		this.drugId = drugId;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public String toString() {
		return "drugId = " + this.drugId + ", Qty = " + this.qty
				+ ", storeId = " + this.storeId + ", branchId = "
				+ this.branchId;
	}
}
