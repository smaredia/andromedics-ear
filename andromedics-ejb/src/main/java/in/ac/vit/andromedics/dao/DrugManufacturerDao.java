package in.ac.vit.andromedics.dao;

import in.ac.vit.andromedics.entities.DrugManufacturer;

import java.util.List;

public interface DrugManufacturerDao extends AbstractDao<DrugManufacturer> {

	public List<DrugManufacturer> getAll();
}
