package in.ac.vit.andromedics.dao;

import in.ac.vit.andromedics.entities.Person;

/**
 * Created with IntelliJ IDEA.
 * User: sahir
 * Date: 31/3/13
 * Time: 4:18 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UserDao extends AbstractDao<Person> {
    public Person getByUsername(String username);
}
