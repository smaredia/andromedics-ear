package in.ac.vit.andromedics.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.sql.Timestamp;

@Generated(value="Dali", date="2013-04-11T18:24:08.619+0530")
@StaticMetamodel(Branch.class)
public class Branch_ {
	public static volatile SingularAttribute<Branch, Status> active;
	public static volatile SingularAttribute<Branch, String> city;
	public static volatile SingularAttribute<Branch, String> closingTiming;
	public static volatile SingularAttribute<Branch, String> country;
	public static volatile SingularAttribute<Branch, Timestamp> createdOn;
	public static volatile SingularAttribute<Branch, String> email;
	public static volatile SingularAttribute<Branch, String> fax;
	public static volatile SingularAttribute<Branch, Integer> id;
	public static volatile SingularAttribute<Branch, String> latitude;
	public static volatile SingularAttribute<Branch, String> longitude;
	public static volatile SingularAttribute<Branch, String> name;
	public static volatile SingularAttribute<Branch, String> openingTiming;
	public static volatile SingularAttribute<Branch, String> phoneNo;
	public static volatile SingularAttribute<Branch, String> pincode;
	public static volatile SingularAttribute<Branch, String> state;
	public static volatile SingularAttribute<Branch, Store> store;
	public static volatile SingularAttribute<Branch, String> street1;
	public static volatile SingularAttribute<Branch, String> street2;
}
