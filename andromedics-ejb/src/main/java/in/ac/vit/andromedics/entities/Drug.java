package in.ac.vit.andromedics.entities;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * The persistent class for the drug database table.
 */
@Entity
@XmlRootElement
@Table(name = "drug")
public class Drug implements Serializable {
	private static final long serialVersionUID = 1L;
	private String code;
	private String constituents;
	private String diseases;
	private DrugManufacturer drugManufacturer;
	private String drugPower;
	private DrugType drugType;
	private Integer id;
	private String name;
	private double price;
	private String quantity;

	public Drug() {
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min = 3)
	public String getCode() {
		return this.code;
	}

	@Column(length = 2147483647)
	public String getConstituents() {
		return this.constituents;
	}

	@Column(length = 2147483647)
	public String getDiseases() {
		return this.diseases;
	}

	// bi-directional many-to-one association to DrugManufacturer
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "drug_manufacturer_id", nullable = false)
	@NotNull
	public DrugManufacturer getDrugManufacturer() {
		return this.drugManufacturer;
	}

	@Column(name = "drug_power", length = 45)
	public String getDrugPower() {
		return this.drugPower;
	}

	// bi-directional many-to-one association to DrugType
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "drug_type_id", nullable = false)
	@NotNull
	public DrugType getDrugType() {
		return this.drugType;
	}

	@Id
	@SequenceGenerator(name="drugSeq", sequenceName="drug_id_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="drugSeq")
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min = 3)
	public String getName() {
		return this.name;
	}

	@Column(nullable = false, precision = 131089)
	@NotNull
	@Min(0)
	public double getPrice() {
		return this.price;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min = 2)
	public String getQuantity() {
		return this.quantity;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setConstituents(String constituents) {
		this.constituents = constituents;
	}

	public void setDiseases(String diseases) {
		this.diseases = diseases;
	}

	public void setDrugManufacturer(DrugManufacturer drugManufacturer) {
		this.drugManufacturer = drugManufacturer;
	}

	public void setDrugPower(String drugPower) {
		this.drugPower = drugPower;
	}

	public void setDrugType(DrugType drugType) {
		this.drugType = drugType;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

}