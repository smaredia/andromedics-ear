package in.ac.vit.andromedics.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.sql.Timestamp;

@Generated(value="Dali", date="2013-04-11T18:24:08.683+0530")
@StaticMetamodel(Invoice.class)
public class Invoice_ {
	public static volatile SingularAttribute<Invoice, Integer> id;
	public static volatile SingularAttribute<Invoice, Branch> branch;
	public static volatile SingularAttribute<Invoice, Timestamp> createdOn;
	public static volatile SingularAttribute<Invoice, String> invoiceNo;
	public static volatile SingularAttribute<Invoice, SalesVia> salesVia;
	public static volatile SingularAttribute<Invoice, Double> totalAmount;
	public static volatile ListAttribute<Invoice, InvoiceProduct> invoiceProducts;
}
