package in.ac.vit.andromedics.dao;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import in.ac.vit.andromedics.entities.Branch;
import in.ac.vit.andromedics.entities.Branch_;
import in.ac.vit.andromedics.entities.Drug;
import in.ac.vit.andromedics.entities.Drug_;
import in.ac.vit.andromedics.entities.Stock;
import in.ac.vit.andromedics.entities.Stock_;

@Stateless
public class StockDaoImpl extends AbstractDaoImpl<Stock> implements StockDao {

	@Inject
	private EntityManager entityManager;

	@Override
	public List<Stock> get(int first, int pageSize, String sortField,
			String sortOrder, Map<String, String> filters) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getCount(Map<String, String> filters) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected EntityManager getEntityManager() {
		return this.entityManager;
	}

	@Override
	public Stock getByDrug(int drugId, int storeId, int branchId) {
		CriteriaBuilder cBuilder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Stock> cQuery = cBuilder.createQuery(Stock.class);
		Root<Stock> stock = cQuery.from(Stock.class);
		Join<Stock, Branch> branch = stock.join(Stock_.branch);
		Join<Stock, Drug> drug = stock.join(Stock_.drug);
		cQuery.where(cBuilder.equal(branch.get(Branch_.id), branchId),
				cBuilder.equal(drug.get(Drug_.id), drugId));
		TypedQuery<Stock> query = this.getEntityManager().createQuery(cQuery);
		try {
			return query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}

}
