package in.ac.vit.andromedics.service;

import in.ac.vit.andromedics.entities.Branch;

import java.util.List;

public interface BranchService extends AbstractService<Branch> {

    List<Branch> getByStore(int storeId);
}
