package in.ac.vit.andromedics.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-04-11T18:24:08.701+0530")
@StaticMetamodel(InvoiceProduct.class)
public class InvoiceProduct_ {
	public static volatile SingularAttribute<InvoiceProduct, Integer> id;
	public static volatile SingularAttribute<InvoiceProduct, Integer> quantity;
	public static volatile SingularAttribute<InvoiceProduct, Double> unitPrice;
	public static volatile SingularAttribute<InvoiceProduct, Drug> drug;
	public static volatile SingularAttribute<InvoiceProduct, Invoice> invoice;
}
