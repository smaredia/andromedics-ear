package in.ac.vit.andromedics.dao;

import in.ac.vit.andromedics.entities.DrugType;
import in.ac.vit.andromedics.entities.DrugType_;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Map;

@Stateless
public class DrugTypeDaoImpl extends AbstractDaoImpl<DrugType> implements
		DrugTypeDao {

	@Inject
	private EntityManager entityManager;

	@Override
	protected EntityManager getEntityManager() {
		return this.entityManager;
	}

	@Override
	public List<DrugType> get(int first, int pageSize, String sortField,
			String sortOrder, Map<String, String> filters) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getCount(Map<String, String> filters) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<DrugType> getAll() {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<DrugType> criteria = builder.createQuery(DrugType.class);
		Root<DrugType> drugType = criteria.from(DrugType.class);
		criteria.orderBy(builder.asc(drugType.get(DrugType_.name)));

		TypedQuery<DrugType> query = this.getEntityManager().createQuery(
				criteria);
		return query.getResultList();
	}

}
