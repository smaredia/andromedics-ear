package in.ac.vit.andromedics.dao;

import in.ac.vit.andromedics.entities.Drug;
import in.ac.vit.andromedics.entities.DrugManufacturer;
import in.ac.vit.andromedics.entities.DrugManufacturer_;
import in.ac.vit.andromedics.entities.DrugType;
import in.ac.vit.andromedics.entities.DrugType_;
import in.ac.vit.andromedics.entities.Drug_;
import org.apache.commons.lang.StringUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Stateless
public class DrugDaoImpl extends AbstractDaoImpl<Drug> implements DrugDao {

	@Inject
	private EntityManager entityManager;

	@Override
	public List<Drug> get(int first, int pageSize, String sortField,
			String sortOrder, Map<String, String> filters) {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Drug> criteria = builder.createQuery(Drug.class);
		Root<Drug> drug = criteria.from(Drug.class);

		this.buildFilters(filters, criteria, builder, drug);

		if (!StringUtils.isBlank(sortField)) {
			Path<?> path = null;
			switch (sortField) {
			case "id":
				path = drug.get(Drug_.id);
				break;
			case "name":
				path = drug.get(Drug_.name);
				break;
			case "code":
				path = drug.get(Drug_.code);
				break;
			case "drugManufacturer":
				Join<Drug, DrugManufacturer> drugManufacturer = drug
						.join(Drug_.drugManufacturer);
				path = drugManufacturer.get(DrugManufacturer_.name);
				break;
			}

			if (sortOrder.equalsIgnoreCase("asc")) {
				criteria.orderBy(builder.asc(path));
			} else {
				criteria.orderBy(builder.desc(path));
			}

		}
		TypedQuery<Drug> query = this.getEntityManager().createQuery(criteria);
		return query.setFirstResult(first).setMaxResults(pageSize)
				.getResultList();
	}

	@Override
	public int getCount(Map<String, String> filters) {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Drug> drug = criteria.from(Drug.class);
		criteria.select(builder.count(drug));

		this.buildFilters(filters, criteria, builder, drug);
		TypedQuery<Long> query = this.getEntityManager().createQuery(criteria);
		return query.getSingleResult().intValue();
	}

	private void buildFilters(Map<String, String> filters,
			CriteriaQuery<?> criteria, CriteriaBuilder builder, Root<Drug> drug) {

		if (filters == null)
			return;

		Set<String> keys = filters.keySet();
		ArrayList<Predicate> predicates = new ArrayList<>();
		for (String key : keys) {
			String value = filters.get(key);
			switch (key) {
			case "id":
				predicates.add(builder.equal(drug.get(Drug_.id), value));
				break;
			case "name":
				predicates.add(builder.like(drug.get(Drug_.name), value + "%"));
				break;
			case "code":
				predicates.add(builder.like(drug.get(Drug_.code), value + "%"));
				break;
			case "drugManufacturer":
				Join<Drug, DrugManufacturer> drugManufacturer = drug
						.join(Drug_.drugManufacturer);
				predicates.add(builder.like(
						drugManufacturer.get(DrugManufacturer_.name), value
								+ "%"));
				break;
			}
		}
		Predicate[] predicateArr = new Predicate[predicates.size()];
		for (int i = 0; i < predicateArr.length; i++) {
			predicateArr[i] = predicates.get(i);
		}
		criteria.where(predicateArr);
	}

	@Override
	public List<Drug> getAll() {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Drug> criteria = builder.createQuery(Drug.class);
		Root<Drug> drugManufacturer = criteria.from(Drug.class);
		criteria.orderBy(builder.asc(drugManufacturer.get(Drug_.name)));

		TypedQuery<Drug> query = this.getEntityManager().createQuery(criteria);
		return query.getResultList();
	}

	@Override
	protected EntityManager getEntityManager() {
		return this.entityManager;
	}

	@Override
	public Drug getByName(String name) {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Drug> criteria = builder.createQuery(Drug.class);
		Root<Drug> drug = criteria.from(Drug.class);
		criteria.where(builder.equal(drug.get(Drug_.name), name));
		TypedQuery<Drug> query = this.getEntityManager().createQuery(criteria);
		try {
			return query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}

	@Override
	public Drug getByCode(String code) {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Drug> criteria = builder.createQuery(Drug.class);
		Root<Drug> drug = criteria.from(Drug.class);
		criteria.where(builder.equal(drug.get(Drug_.code), code));
		TypedQuery<Drug> query = this.getEntityManager().createQuery(criteria);
		try {
			return query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}

	@Override
	public List<Drug> getByManufacturer(int id) {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Drug> criteriaQuery = builder.createQuery(Drug.class);
		Root<Drug> drug = criteriaQuery.from(Drug.class);
		Join<Drug, DrugManufacturer> manufacturer = drug
				.join(Drug_.drugManufacturer);
		criteriaQuery.where(builder.equal(
				manufacturer.get(DrugManufacturer_.id), id));
		TypedQuery<Drug> query = this.getEntityManager().createQuery(
				criteriaQuery);
		return query.getResultList();
	}

	@Override
	public List<Drug> getByManufacturerAndType(int manufacturerId, int typeId) {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Drug> criteriaQuery = builder.createQuery(Drug.class);
		Root<Drug> drug = criteriaQuery.from(Drug.class);
		Join<Drug, DrugManufacturer> manufacturer = drug
				.join(Drug_.drugManufacturer);
		Join<Drug, DrugType> drugType = drug.join(Drug_.drugType);
		criteriaQuery.where(builder.equal(
				manufacturer.get(DrugManufacturer_.id), manufacturerId),
				builder.equal(drugType.get(DrugType_.id), typeId));
		TypedQuery<Drug> query = this.getEntityManager().createQuery(
				criteriaQuery);
		return query.getResultList();
	}

	@Override
	public List<Drug> getNameLike(String name) {
		CriteriaBuilder criteriaBuilder = this.getEntityManager()
				.getCriteriaBuilder();
		CriteriaQuery<Drug> criteriaQuery = criteriaBuilder
				.createQuery(Drug.class);
		Root<Drug> drug = criteriaQuery.from(Drug.class);
		criteriaQuery.where(criteriaBuilder.like(
				criteriaBuilder.lower(drug.get(Drug_.name)), name.toLowerCase()
						+ "%"));
		criteriaQuery.orderBy(criteriaBuilder.asc(drug.get(Drug_.name)));
		TypedQuery<Drug> query = this.getEntityManager().createQuery(
				criteriaQuery);
		return query.getResultList();
	}

}
