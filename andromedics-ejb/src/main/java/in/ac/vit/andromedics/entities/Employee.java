package in.ac.vit.andromedics.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Entity implementation class for Entity: Employee
 */
@Entity
@XmlRootElement
public class Employee extends Person implements Serializable {

	private Branch branch;
	private Store store;

	private static final long serialVersionUID = 1L;

	public Employee() {
		super();
	}

	// bi-directional many-to-one association to Branch
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "branch_id")
	@NotNull
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	// bi-directional many-to-one association to Branch
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "store_id")
	@NotNull
	public Store getStore() {
		return this.store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

}
