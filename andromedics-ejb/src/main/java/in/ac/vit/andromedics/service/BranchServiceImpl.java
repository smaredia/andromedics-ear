package in.ac.vit.andromedics.service;

import in.ac.vit.andromedics.dao.BranchDao;
import in.ac.vit.andromedics.dao.StoreDao;
import in.ac.vit.andromedics.entities.Branch;
import in.ac.vit.andromedics.entities.Employee;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.exceptions.ValidationException;
import in.ac.vit.andromedics.security.AndroMedicsPrincipal;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.*;

@Stateless
public class BranchServiceImpl implements BranchService {

    @Inject
    private BranchDao branchDao;
    @Inject
    private StoreDao storeDao;
    @Inject
    private Validator validator;
    @Resource
    private SessionContext context;

    @Override
    @RolesAllowed({UserRoles.ADMIN})
    public Branch save(Branch branch) {
        Set<ConstraintViolation<Branch>> violations = this.getValidator()
                .validate(branch);
        if (!violations.isEmpty()) {
            throw new ValidationException(new HashSet<ConstraintViolation<?>>(
                    violations));
        }
        branch = this.getBranchDao().save(branch);
        return branch;
    }

    @Override
    public Branch update(Branch branch) {
        Set<ConstraintViolation<Branch>> violations = this.getValidator()
                .validate(branch);
        if (!violations.isEmpty()) {
            throw new ValidationException(new HashSet<ConstraintViolation<?>>(
                    violations));
        }
        branch = this.getBranchDao().update(branch);
        return branch;
    }

    @Override
    @RolesAllowed({UserRoles.ADMIN})
    public void delete(Branch branch) {
        if (branch != null) {
            this.getBranchDao().delete(branch);
        }
    }

    @Override
    @RolesAllowed({UserRoles.ADMIN})
    public void delete(int id) {
        if (id > 0) {
            this.getBranchDao().deleteById(id);
        }
    }

    @Override
    @RolesAllowed({UserRoles.ADMIN, UserRoles.EMPLOYEE, UserRoles.MANAGER})
    public int getCount() {
        if (!context.isCallerInRole(UserRoles.ADMIN)) {
            Map<String, String> filters = new HashMap<String, String>(1);
            Employee employee = (Employee) ((AndroMedicsPrincipal) context
                    .getCallerPrincipal()).getUser();
            filters.put("store", employee.getStore().getName());
            return this.getCount(filters);
        } else {
            return this.getBranchDao().getCount().intValue();
        }
    }

    @Override
    public Branch getById(int id) {
        if (id > 0) {
            return this.getBranchDao().getById(id);
        } else {
            return null;
        }
    }

    @Override
    @RolesAllowed({UserRoles.ADMIN, UserRoles.EMPLOYEE, UserRoles.MANAGER})
    public List<Branch> get(int first, int pageSize, String sortField,
                            String sortOrder, Map<String, String> filters) {
        if (!context.isCallerInRole(UserRoles.ADMIN)) {
            Employee employee = (Employee) ((AndroMedicsPrincipal) context
                    .getCallerPrincipal()).getUser();
            filters.put("store", employee.getStore().getName());
        }
        return this.getBranchDao().get(first, pageSize, sortField, sortOrder,
                filters);
    }

    @Override
    @RolesAllowed({UserRoles.ADMIN, UserRoles.EMPLOYEE, UserRoles.MANAGER})
    public int getCount(Map<String, String> filters) {
        if (!context.isCallerInRole(UserRoles.ADMIN)) {
            Employee employee = (Employee) ((AndroMedicsPrincipal) context
                    .getCallerPrincipal()).getUser();
            filters.put("store", employee.getStore().getName());
        }
        return this.getBranchDao().getCount(filters);
    }

    protected BranchDao getBranchDao() {
        return branchDao;
    }

    protected StoreDao getStoreDao() {
        return storeDao;
    }

    protected Validator getValidator() {
        return this.validator;
    }

    @Override
    public List<Branch> getByStore(int storeId) {
        if (storeId > 0) {
            return this.branchDao.getByStore(storeId);
        } else {
            return null;
        }
    }
}
