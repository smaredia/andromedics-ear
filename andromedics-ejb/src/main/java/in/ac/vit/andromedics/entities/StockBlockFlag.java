package in.ac.vit.andromedics.entities;

public enum StockBlockFlag {
	FAILED, BLOCKED, COMPLETED;
}
