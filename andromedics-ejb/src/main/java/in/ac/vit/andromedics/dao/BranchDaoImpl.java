package in.ac.vit.andromedics.dao;

import in.ac.vit.andromedics.entities.*;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Stateless
public class BranchDaoImpl extends AbstractDaoImpl<Branch> implements BranchDao {

    @Inject
    private EntityManager entityManager;
    private transient Logger log = Logger.getLogger(getClass());

    @Override
    public List<Branch> get(int first, int pageSize, String sortField,
                            String sortOrder, Map<String, String> filters) {
        CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Branch> criteria = builder.createQuery(Branch.class);
        Root<Branch> branch = criteria.from(Branch.class);

        this.buildFilters(filters, criteria, builder, branch);

        if (!StringUtils.isBlank(sortField)) {
            Path<?> path = null;
            switch (sortField) {
                case "id":
                    path = branch.get(Branch_.id);
                    break;
                case "name":
                    path = branch.get(Branch_.name);
                    break;
                case "city":
                    path = branch.get(Branch_.city);
                    break;
                case "state":
                    path = branch.get(Branch_.state);
                    break;
                case "active":
                    path = branch.get(Branch_.active);
                case "store":
                    Join<Branch, Store> store = branch.join(Branch_.store);
                    path = store.get(Store_.name);
                    break;
            }

            if (sortOrder.equalsIgnoreCase("asc")) {
                criteria.orderBy(builder.asc(path));
            } else {
                criteria.orderBy(builder.desc(path));
            }

        }
        TypedQuery<Branch> query = this.getEntityManager()
                .createQuery(criteria);
        return query.setFirstResult(first).setMaxResults(pageSize)
                .getResultList();
    }

    private void buildFilters(Map<String, String> filters,
                              CriteriaQuery<?> criteria, CriteriaBuilder builder,
                              Root<Branch> branch) {

        if (filters == null)
            return;

        Set<String> keys = filters.keySet();
        ArrayList<Predicate> predicates = new ArrayList<>();
        for (String key : keys) {
            String value = filters.get(key);
            switch (key) {
                case "id":
                    predicates.add(builder.equal(branch.get(Branch_.id), value));
                    break;
                case "name":
                    predicates.add(builder.like(branch.get(Branch_.name), value
                            + "%"));
                    break;
                case "city":
                    predicates.add(builder.like(branch.get(Branch_.city), value
                            + "%"));
                    break;
                case "state":
                    predicates.add(builder.like(branch.get(Branch_.state), value
                            + "%"));
                    break;
                case "active":
                    predicates.add(builder.equal(branch.get(Branch_.active),
                            Status.valueOf(value)));
                    break;
                case "store":
                    Join<Branch, Store> store = branch.join(Branch_.store);
                    predicates.add(builder.like(store.get(Store_.name), "%" + value
                            + "%"));
                    log.debug("Store Name = " + value);
            }
        }
        Predicate[] predicateArr = new Predicate[predicates.size()];
        for (int i = 0; i < predicateArr.length; i++) {
            predicateArr[i] = predicates.get(i);
        }
        criteria.where(predicateArr);
    }

    @Override
    public int getCount(Map<String, String> filters) {
        CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
        Root<Branch> branch = criteria.from(Branch.class);
        criteria.select(builder.count(branch));

        this.buildFilters(filters, criteria, builder, branch);
        TypedQuery<Long> query = this.getEntityManager().createQuery(criteria);
        return query.getSingleResult().intValue();
    }

    @Override
    protected EntityManager getEntityManager() {
        return this.entityManager;
    }

    @Override
    public List<Branch> getByStore(int storeId) {
        CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Branch> criteriaQuery = builder.createQuery(Branch.class);
        Root<Branch> branch = criteriaQuery.from(Branch.class);
        Join<Branch, Store> store = branch.join(Branch_.store);
        criteriaQuery.where(builder.equal(store.get(Store_.id), storeId));

        TypedQuery<Branch> query = this.getEntityManager().createQuery(criteriaQuery);
        return query.getResultList();
    }
}
