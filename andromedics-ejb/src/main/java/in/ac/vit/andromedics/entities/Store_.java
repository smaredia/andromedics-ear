package in.ac.vit.andromedics.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.sql.Timestamp;
import java.util.Date;

@Generated(value="Dali", date="2013-04-11T18:24:08.781+0530")
@StaticMetamodel(Store.class)
public class Store_ {
	public static volatile SingularAttribute<Store, Integer> id;
	public static volatile SingularAttribute<Store, Status> active;
	public static volatile SingularAttribute<Store, String> city;
	public static volatile SingularAttribute<Store, String> country;
	public static volatile SingularAttribute<Store, Timestamp> createdOn;
	public static volatile SingularAttribute<Store, String> email;
	public static volatile SingularAttribute<Store, String> fax;
	public static volatile SingularAttribute<Store, String> name;
	public static volatile SingularAttribute<Store, String> phoneNo;
	public static volatile SingularAttribute<Store, String> pincode;
	public static volatile SingularAttribute<Store, String> state;
	public static volatile SingularAttribute<Store, String> street1;
	public static volatile SingularAttribute<Store, String> street2;
	public static volatile SingularAttribute<Store, Date> subscriptionDate;
	public static volatile SingularAttribute<Store, String> website;
	public static volatile ListAttribute<Store, Branch> branches;
}
