package in.ac.vit.andromedics.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

@Generated(value="Dali", date="2013-04-04T18:54:37.007+0530")
@StaticMetamodel(Person.class)
public class Person_ {
	public static volatile SingularAttribute<Person, String> address;
	public static volatile SingularAttribute<Person, Date> dateOfBirth;
	public static volatile SingularAttribute<Person, String> email;
	public static volatile SingularAttribute<Person, String> firstName;
	public static volatile SingularAttribute<Person, Integer> id;
	public static volatile SingularAttribute<Person, String> lastName;
	public static volatile SingularAttribute<Person, String> middleName;
	public static volatile SingularAttribute<Person, String> password;
	public static volatile SingularAttribute<Person, String> phoneNo;
	public static volatile SingularAttribute<Person, String> role;
	public static volatile SingularAttribute<Person, String> username;
}
