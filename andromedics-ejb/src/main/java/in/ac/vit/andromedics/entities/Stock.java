package in.ac.vit.andromedics.entities;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * The persistent class for the stock database table.
 */
@Entity
@XmlRootElement
@Table(name = "stock")
public class Stock implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer quantity;
	private Integer reorderLevel = 0;
	private Branch branch;
	private Drug drug;

	public Stock() {
	}

	@Id
	@SequenceGenerator(name = "stockSeq", sequenceName = "stock_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "stockSeq")
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false)
	@NotNull
	@Min(1)
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name = "reorder_level", nullable = false)
	@NotNull
	@Min(0)
	public Integer getReorderLevel() {
		return this.reorderLevel;
	}

	public void setReorderLevel(Integer reorderLevel) {
		this.reorderLevel = reorderLevel;
	}

	// bi-directional many-to-one association to Branch
	@ManyToOne
	@JoinColumn(name = "branch_id", nullable = false)
	@NotNull
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	// bi-directional many-to-one association to Drug
	@ManyToOne
	@JoinColumn(name = "drug_id", nullable = false)
	@NotNull
	public Drug getDrug() {
		return this.drug;
	}

	public void setDrug(Drug drug) {
		this.drug = drug;
	}

}