package in.ac.vit.andromedics.service;

import in.ac.vit.andromedics.entities.DrugManufacturer;

import java.util.List;

public interface DrugManufacturerService extends AbstractService<DrugManufacturer> {
	public List<DrugManufacturer> getAll();
}
