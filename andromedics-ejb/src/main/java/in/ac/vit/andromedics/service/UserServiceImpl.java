package in.ac.vit.andromedics.service;

import in.ac.vit.andromedics.dao.UserDao;
import in.ac.vit.andromedics.entities.Person;
import in.ac.vit.andromedics.exceptions.ValidationException;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jboss.security.auth.spi.Util;

@Stateless
public class UserServiceImpl implements UserService {

	@Inject
	private UserDao userDao;
	@Inject
	private Validator validator;
	private transient Logger log = Logger.getLogger(getClass());

	@Override
	public Person authenticate(String username, String password) {
		Person p = this.getUserDao().getByUsername(username);
		if (p != null) {
			if (!p.getPassword()
					.equals(Util.createPasswordHash("MD5", "hex", null, null,
							password))) {
				p = null;
			}
		}
		return p;
	}

	@Override
	public Person save(Person person) {
		Set<ConstraintViolation<Person>> violations = this.validator
				.validate(person);
		ValidationException exp = new ValidationException(
				new HashSet<ConstraintViolation<?>>(violations));

		if (!StringUtils.isEmpty(person.getUsername())) {
			Person oldPerson = this.userDao.getByUsername(person.getUsername());
			if (oldPerson != null) {
				exp.addError("username", "username already exists");
			}
		}
		if (!exp.isEmpty()) {
			throw exp;
		}
		person.setPassword(Util.createPasswordHash("MD5", "hex", null, null,
				person.getPassword()));
		person = this.userDao.save(person);
		return person;
	}

	@Override
	public Person update(Person person) {
		Set<ConstraintViolation<Person>> violations = this.validator
				.validate(person);
		ValidationException exp = new ValidationException(
				new HashSet<ConstraintViolation<?>>(violations));

		if (!StringUtils.isEmpty(person.getUsername())) {
			Person oldPerson = this.userDao.getByUsername(person.getUsername());
			if (oldPerson != null && oldPerson.getId() != person.getId()) {
				log.debug(oldPerson.getId() + " " + person.getId());
				exp.addError("username", "username already exists");
			}
		}
		if (!exp.isEmpty()) {
			throw exp;
		}
		person.setPassword(Util.createPasswordHash("MD5", "hex", null, null,
				person.getPassword()));
		person = this.userDao.update(person);
		return person;
	}

	@Override
	public void delete(Person person) {
		if (person != null) {
			this.userDao.delete(person);
		}
	}

	@Override
	public List<Person> get(int first, int pageSize, String sortField,
			String sortOrder, Map<String, String> filters) {
		return this.getUserDao().get(first, pageSize, sortField, sortOrder,
				filters);
	}

	@Override
	public int getCount() {
		return this.getUserDao().getCount().intValue();
	}

	protected UserDao getUserDao() {
		return this.userDao;
	}

	@Override
	public void delete(int id) {
		Person p = this.getById(id);
		if (p != null) {
			this.delete(p);
		}
	}

	@Override
	public int getCount(Map<String, String> filters) {
		return this.userDao.getCount(filters);
	}

	public Person getById(int id) {
		return this.getUserDao().getById(id);
	}

	@Override
	public Person getByUsername(String username) {
		if (StringUtils.isEmpty(username)) {
			return null;
		} else {
			return this.getUserDao().getByUsername(username);
		}
	}
}
