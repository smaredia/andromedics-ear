package in.ac.vit.andromedics.entities;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The persistent class for the branch database table.
 */
@Entity
@XmlRootElement
@Table(name = "branch")
public class Branch implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Status active;
	private String city;
	private String closingTiming;
	private String country;
	private Timestamp createdOn;
	private String email;
	private String fax;
	private String latitude;
	private String longitude;
	private String name;
	private String openingTiming;
	private String phoneNo;
	private String pincode;
	private String state;
	private String street1;
	private String street2;
	private Store store;

	public Branch() {
	}

	@Column(nullable = false)
	@NotNull
	@Enumerated(EnumType.ORDINAL)
	public Status getActive() {
		return this.active;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min = 2)
	public String getCity() {
		return this.city;
	}

	@Column(name = "closing_timing", length = 50)
	@NotNull
	@Pattern(regexp = "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", message = "{time_format}")
	public String getClosingTiming() {
		return this.closingTiming;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min = 3)
	public String getCountry() {
		return this.country;
	}

	@Column(name = "created_on", nullable = false)
	@NotNull
	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Email
	public String getEmail() {
		return this.email;
	}

	@Column(nullable = false, length = 20)
	@Pattern(regexp = "^[0-9]{3}(-[0-9]{4}){2}$", message = "{phone}")
	public String getFax() {
		return this.fax;
	}

	@Id
	@SequenceGenerator(name="branchSeq", sequenceName="branch_id_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="branchSeq")
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	@Column(nullable = false, length = 45)
	@NotNull
	public String getLatitude() {
		return this.latitude;
	}

	@Column(nullable = false, length = 45)
	@NotNull
	public String getLongitude() {
		return this.longitude;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min = 2)
	public String getName() {
		return this.name;
	}

	@Column(name = "opening_timing", length = 50)
	@NotNull
	@Pattern(regexp="^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", message="{time_format}")
	public String getOpeningTiming() {
		return this.openingTiming;
	}

	@Column(name = "phone_no", nullable = false, length = 20)
	@NotNull
	@Pattern(regexp = "^[0-9]{3}(-[0-9]{4}){2}$", message = "{phone}")
	public String getPhoneNo() {
		return this.phoneNo;
	}

	@Column(nullable = false, length = 20)
	@NotNull
	@Size(min=5, max=8)
	@Pattern(regexp="^[0-9]*$", message="{pincode}")
	public String getPincode() {
		return this.pincode;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min=2)
	public String getState() {
		return this.state;
	}

	// bi-directional many-to-one association to Store
	@ManyToOne
	@JoinColumn(name = "store_id", nullable = false)
	@NotNull
	public Store getStore() {
		return this.store;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min=2)
	public String getStreet1() {
		return this.street1;
	}

	@Column(length = 255)
	public String getStreet2() {
		return this.street2;
	}

	public void setActive(Status active) {
		this.active = active;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setClosingTiming(String closingTiming) {
		this.closingTiming = closingTiming;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOpeningTiming(String openingTiming) {
		this.openingTiming = openingTiming;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

}