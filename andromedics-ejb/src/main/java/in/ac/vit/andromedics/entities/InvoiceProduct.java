package in.ac.vit.andromedics.entities;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * The persistent class for the invoice_product database table.
 */
@Entity
@XmlRootElement
@Table(name = "invoice_product")
public class InvoiceProduct implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer quantity;
	private double unitPrice;
	private Drug drug;
	private Invoice invoice;

	public InvoiceProduct() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false)
	@NotNull
	@Min(1)
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name = "unit_price", nullable = false, precision = 131089)
	@NotNull
	@Min(0)
	public double getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	// bi-directional many-to-one association to Drug
	@ManyToOne
	@JoinColumn(name = "drug_id", nullable = false)
	@NotNull
	public Drug getDrug() {
		return this.drug;
	}

	public void setDrug(Drug drug) {
		this.drug = drug;
	}

	// bi-directional many-to-one association to Invoice
	@ManyToOne
	@JoinColumn(name = "invoice_id", nullable = false)
	@NotNull
	public Invoice getInvoice() {
		return this.invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

}