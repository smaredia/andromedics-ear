package in.ac.vit.andromedics.entities;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * The persistent class for the stock_block_product database table.
 */
@Entity
@XmlRootElement
@Table(name = "stock_block_product")
public class StockBlockProduct implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer quantity;
	private Drug drug;
	private StockBlock stockBlock;

	public StockBlockProduct() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false)
	@NotNull
	@Min(1)
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	// bi-directional many-to-one association to Drug
	@ManyToOne
	@JoinColumn(name = "drug_id", nullable = false)
	@NotNull
	public Drug getDrug() {
		return this.drug;
	}

	public void setDrug(Drug drug) {
		this.drug = drug;
	}

	// bi-directional many-to-one association to StockBlock
	@ManyToOne
	@JoinColumn(name = "stock_block_id", nullable = false)
	@NotNull
	public StockBlock getStockBlock() {
		return this.stockBlock;
	}

	public void setStockBlock(StockBlock stockBlock) {
		this.stockBlock = stockBlock;
	}

}