package in.ac.vit.andromedics.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.sql.Timestamp;

@Generated(value="Dali", date="2013-04-11T18:24:08.716+0530")
@StaticMetamodel(StockBlock.class)
public class StockBlock_ {
	public static volatile SingularAttribute<StockBlock, Integer> id;
	public static volatile SingularAttribute<StockBlock, Timestamp> blockedTime;
	public static volatile SingularAttribute<StockBlock, Branch> branch;
	public static volatile SingularAttribute<StockBlock, String> code;
	public static volatile SingularAttribute<StockBlock, Timestamp> createdOn;
	public static volatile SingularAttribute<StockBlock, StockBlockFlag> flag;
	public static volatile SingularAttribute<StockBlock, Person> person;
	public static volatile ListAttribute<StockBlock, StockBlockProduct> stockBlockProducts;
}
