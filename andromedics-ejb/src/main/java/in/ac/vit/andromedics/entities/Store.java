package in.ac.vit.andromedics.entities;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the store database table.
 */
@Entity
@XmlRootElement
@Table(name = "store")
@NamedQueries({
		@NamedQuery(name = "Store.findAll", query = "SELECT s FROM Store s"),
		@NamedQuery(name = "Store.count", query = "SELECT count(s.id) FROM Store s") })
public class Store implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Status active;
	private String city;
	private String country;
	private Timestamp createdOn;
	private String email;
	private String fax;
	private String name;
	private String phoneNo;
	private String pincode;
	private String state;
	private String street1;
	private String street2;
	private Date subscriptionDate;
	private String website;
	private List<Branch> branches;

	public Store() {
	}

	@Id
	@SequenceGenerator(name = "storeSeq", sequenceName = "store_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "storeSeq")
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false)
	@NotNull
	@Enumerated(EnumType.ORDINAL)
	public Status getActive() {
		return this.active;
	}

	public void setActive(Status active) {
		this.active = active;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min = 2, max = 255)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min = 2, max = 255)
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "created_on", nullable = false)
	@NotNull
	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Email
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(length = 20)
	@Pattern(regexp = "^[0-9]{3}(-[0-9]{4}){2}$", message = "{phone}")
	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min = 3, max = 255)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "phone_no", nullable = false, length = 20)
	@NotNull
	@Pattern(regexp = "^[0-9]{3}(-[0-9]{4}){2}$", message = "{phone}")
	public String getPhoneNo() {
		return this.phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@Column(nullable = false, length = 45)
	@NotNull
	@Pattern(regexp = "^[0-9]*$", message = "{pincode}")
	@Size(min = 5, max = 8)
	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min = 2, max = 255)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(nullable = false, length = 2147483647)
	@NotNull
	@Size(min = 2)
	public String getStreet1() {
		return this.street1;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	@Column(length = 2147483647)
	public String getStreet2() {
		return this.street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	@Column(name = "subscription_date", nullable = false)
	@NotNull
	public Date getSubscriptionDate() {
		return this.subscriptionDate;
	}

	public void setSubscriptionDate(Date subscriptionDate) {
		this.subscriptionDate = subscriptionDate;
	}

	@Column(length = 255)
	@Size(min = 5, max = 255)
	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	// bi-directional many-to-one association to Branch
	@OneToMany(mappedBy = "store")
	public List<Branch> getBranches() {
		return this.branches;
	}

	public void setBranches(List<Branch> branches) {
		this.branches = branches;
	}

	public Branch addBranch(Branch branch) {
		this.getBranches().add(branch);
		branch.setStore(this);

		return branch;
	}

	public Branch removeBranch(Branch branch) {
		this.getBranches().remove(branch);
		branch.setStore(null);

		return branch;
	}

}