package in.ac.vit.andromedics.service;

import in.ac.vit.andromedics.entities.Person;

/**
 * Created with IntelliJ IDEA.
 * User: sahir
 * Date: 31/3/13
 * Time: 4:41 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UserService extends AbstractService<Person> {
    public Person authenticate(String username, String password);
    public Person getById(int id);
    public Person getByUsername(String username);
}
