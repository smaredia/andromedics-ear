package in.ac.vit.andromedics.entities;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the invoice database table.
 */
@Entity
@XmlRootElement
@Table(name = "invoice")
public class Invoice implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private Branch branch;
	private Timestamp createdOn;
	private String invoiceNo;
	private SalesVia salesVia;
	private double totalAmount;
	private List<InvoiceProduct> invoiceProducts;

	public Invoice() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	// bi-directional many-to-one association to Branch
	@ManyToOne
	@JoinColumn(name = "branch_id", nullable = false)
	@NotNull
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@Column(name = "created_on", nullable = false)
	@NotNull
	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "invoice_no", nullable = false, length = 45)
	@NotNull
	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	@Column(name = "sales_via", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	@NotNull
	public SalesVia getSalesVia() {
		return this.salesVia;
	}

	public void setSalesVia(SalesVia salesVia) {
		this.salesVia = salesVia;
	}

	@Column(name = "total_amount", nullable = false, precision = 131089)
	@NotNull
	@Min(0)
	public double getTotalAmount() {
		return this.totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	// bi-directional many-to-one association to InvoiceProduct
	@OneToMany(mappedBy = "invoice")
	@NotNull
	public List<InvoiceProduct> getInvoiceProducts() {
		return this.invoiceProducts;
	}

	public void setInvoiceProducts(List<InvoiceProduct> invoiceProducts) {
		this.invoiceProducts = invoiceProducts;
	}

	public InvoiceProduct addInvoiceProduct(InvoiceProduct invoiceProduct) {
		getInvoiceProducts().add(invoiceProduct);
		invoiceProduct.setInvoice(this);

		return invoiceProduct;
	}

	public InvoiceProduct removeInvoiceProduct(InvoiceProduct invoiceProduct) {
		getInvoiceProducts().remove(invoiceProduct);
		invoiceProduct.setInvoice(null);

		return invoiceProduct;
	}

}