package in.ac.vit.andromedics.dao;

import in.ac.vit.andromedics.entities.Person;
import in.ac.vit.andromedics.entities.Person_;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA. User: sahir Date: 31/3/13 Time: 4:19 PM To change
 * this template use File | Settings | File Templates.
 */
@Stateless
public class UserDaoImpl extends AbstractDaoImpl<Person> implements UserDao {

	@Inject
	private EntityManager entityManager;
	private transient Logger log = Logger.getLogger(getClass());

	@Override
	public List<Person> get(int first, int pageSize, String sortField,
			String sortOrder, Map<String, String> filters) {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Person> criteria = builder.createQuery(Person.class);
		Root<Person> person = criteria.from(Person.class);

		this.buildFilters(filters, criteria, builder, person);

		if (!StringUtils.isBlank(sortField)) {
			Path<?> path = null;
			switch (sortField) {
			case "id":
				path = person.get(Person_.id);
				break;
			case "firstName":
				path = person.get(Person_.firstName);
				break;
			case "lastName":
				path = person.get(Person_.lastName);
				break;
			case "email":
				path = person.get(Person_.email);
				break;
			case "role":
				path = person.get(Person_.role);
				break;
			}

			if (sortOrder.equalsIgnoreCase("asc")) {
				criteria.orderBy(builder.asc(path));
			} else {
				criteria.orderBy(builder.desc(path));
			}

		}
		TypedQuery<Person> query = this.getEntityManager()
				.createQuery(criteria);
		return query.setFirstResult(first).setMaxResults(pageSize)
				.getResultList();

	}

	@Override
	public Person getByUsername(String username) {
		Person p = null;
		try {
			p = this.getEntityManager()
					.createNamedQuery("Person.findByUsername", Person.class)
					.setParameter("username", username).getSingleResult();
		} catch (NoResultException ex) {
			log.error("No User found for " + username);
		}
		return p;
	}

	@Override
	protected EntityManager getEntityManager() {
		return this.entityManager;
	}

	@Override
	public int getCount(Map<String, String> filters) {
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Person> person = criteria.from(Person.class);
		criteria.select(builder.count(person));

		this.buildFilters(filters, criteria, builder, person);
		TypedQuery<Long> query = this.getEntityManager().createQuery(criteria);
		return query.getSingleResult().intValue();
	}

	private void buildFilters(Map<String, String> filters,
			CriteriaQuery<?> criteria, CriteriaBuilder builder,
			Root<Person> person) {

		if (filters == null)
			return;

		Set<String> keys = filters.keySet();
		ArrayList<Predicate> predicates = new ArrayList<>();
		for (String key : keys) {
			String value = filters.get(key);
			switch (key) {
			case "id":
				predicates.add(builder.equal(person.get(Person_.id), value));
				break;
			case "firstName":
				predicates.add(builder.like(person.get(Person_.firstName),
						value + "%"));
				break;
			case "lastName":
				predicates.add(builder.like(person.get(Person_.lastName), value
						+ "%"));
				break;
			case "email":
				predicates.add(builder.like(person.get(Person_.email), value
						+ "%"));
				break;
			case "role":
				predicates.add(builder.equal(person.get(Person_.role), value));
				break;
			}
		}
		Predicate[] predicateArr = new Predicate[predicates.size()];
		for (int i = 0; i < predicateArr.length; i++) {
			predicateArr[i] = predicates.get(i);
		}
		criteria.where(predicateArr);
	}

}
