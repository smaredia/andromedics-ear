package in.ac.vit.andromedics.dao;

import in.ac.vit.andromedics.entities.Branch;

import java.util.List;

public interface BranchDao extends AbstractDao<Branch> {
    List<Branch> getByStore(int storeId);
}
