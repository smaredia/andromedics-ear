package in.ac.vit.andromedics.service;

import java.util.List;

import in.ac.vit.andromedics.entities.Stock;
import in.ac.vit.andromedics.entities.form.DrugBean;

public interface StockService extends AbstractService<Stock> {

	void addStock(List<DrugBean> drugs);

	Stock getByDrug(int drugId, int storeId, int branchId);
}
