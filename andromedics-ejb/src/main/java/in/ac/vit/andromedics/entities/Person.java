package in.ac.vit.andromedics.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the person database table.
 */
@Entity
@Table(name = "person")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "person_type", discriminatorType = DiscriminatorType.STRING)
@NamedQueries({
		@NamedQuery(name = "Person.findByUsername", query = "SELECT p FROM Person p WHERE p.username = :username"),
		@NamedQuery(name = "Person.count", query = "SELECT COUNT(p.id) FROM Person p") })
public abstract class Person implements Serializable {
	private static final long serialVersionUID = 1L;
	private String address;
	private Date dateOfBirth;
	private String email;
	private String firstName;
	private Integer id;
	private String lastName;
	private String middleName;
	private String password;
	private String phoneNo;
	private String role;
	private String username;

	public Person() {
	}

	@Column(length = 2147483647)
	public String getAddress() {
		return this.address;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_birth")
	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	@Column(length = 255)
	public String getEmail() {
		return this.email;
	}

	@Column(name = "first_name", nullable = false, length = 255)
	@NotNull
	@Size(min = 2)
	public String getFirstName() {
		return this.firstName;
	}

	@Id
	@SequenceGenerator(name="personSeq", sequenceName="person_id_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="personSeq")
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	@Column(name = "last_name", nullable = false, length = 255)
	@NotNull
	@Size(min = 2)
	public String getLastName() {
		return this.lastName;
	}

	@Column(name = "middle_name", length = 255)
	public String getMiddleName() {
		return this.middleName;
	}

	@Column(nullable = false, length = 2147483647)
	@NotNull
	@Size(min = 6)
	public String getPassword() {
		return this.password;
	}

	@Column(name = "phone_no", length = 20)
	@Pattern(regexp = "^[0-9]{2}-[0-9]{10}$", message = "{mobile}")
	public String getPhoneNo() {
		return this.phoneNo;
	}

	@Column(nullable = false, length = 45)
	@NotNull
	public String getRole() {
		return this.role;
	}

	@Column(nullable = false, length = 255)
	@NotNull
	@Size(min = 5, max = 20)
	public String getUsername() {
		return this.username;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String fullName() {
		return this.getFirstName() + " " + this.getLastName();
	}

}