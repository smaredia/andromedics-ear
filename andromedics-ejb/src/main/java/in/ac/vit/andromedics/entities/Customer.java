package in.ac.vit.andromedics.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Entity implementation class for Entity: User
 */
@Entity
@XmlRootElement
public class Customer extends Person implements Serializable {

    private String phoneType;

    private static final long serialVersionUID = 1L;

    public Customer() {
        super();
    }

    @Column(name = "phone_type", length = 100)
    public String getPhoneType() {
        return this.phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

}
