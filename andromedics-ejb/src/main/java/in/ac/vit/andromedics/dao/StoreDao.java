/**
 *
 */
package in.ac.vit.andromedics.dao;

import in.ac.vit.andromedics.entities.Store;

import java.util.List;

/**
 * @author sahir
 */
public interface StoreDao extends AbstractDao<Store> {
    public List<Store> getAll();

}
