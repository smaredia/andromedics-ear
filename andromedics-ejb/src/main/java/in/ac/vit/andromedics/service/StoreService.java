package in.ac.vit.andromedics.service;

import in.ac.vit.andromedics.entities.Store;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sahir
 * Date: 1/4/13
 * Time: 12:13 AM
 * To change this template use File | Settings | File Templates.
 */
public interface StoreService extends AbstractService<Store> {

	List<Store> getAll();

}
