package in.ac.vit.andromedics.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the stock_block database table.
 */
@Entity
@XmlRootElement
@Table(name = "stock_block")
public class StockBlock implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Timestamp blockedTime;
	private Branch branch;
	private String code;
	private Timestamp createdOn;
	private StockBlockFlag flag;
	private Person person;
	private List<StockBlockProduct> stockBlockProducts;

	public StockBlock() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "blocked_time", nullable = false)
	@NotNull
	public Timestamp getBlockedTime() {
		return this.blockedTime;
	}

	public void setBlockedTime(Timestamp blockedTime) {
		this.blockedTime = blockedTime;
	}

	// bi-directional many-to-one association to Branch
	@ManyToOne
	@JoinColumn(name = "branch_id", nullable = false)
	@NotNull
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@Column(nullable = false, length = 45)
	@NotNull
	@Size(min = 6)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "created_on", nullable = false)
	@NotNull
	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	@Column(nullable = false)
	@Enumerated(EnumType.ORDINAL)
	@NotNull
	public StockBlockFlag getFlag() {
		return this.flag;
	}

	public void setFlag(StockBlockFlag flag) {
		this.flag = flag;
	}

	// bi-directional many-to-one association to Person
	@ManyToOne
	@JoinColumn(name = "person_id", nullable = false)
	@NotNull
	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	// bi-directional many-to-one association to StockBlockProduct
	@OneToMany(mappedBy = "stockBlock")
	@NotNull
	@Size(min = 1)
	public List<StockBlockProduct> getStockBlockProducts() {
		return this.stockBlockProducts;
	}

	public void setStockBlockProducts(List<StockBlockProduct> stockBlockProducts) {
		this.stockBlockProducts = stockBlockProducts;
	}

	public StockBlockProduct addStockBlockProduct(
			StockBlockProduct stockBlockProduct) {
		getStockBlockProducts().add(stockBlockProduct);
		stockBlockProduct.setStockBlock(this);

		return stockBlockProduct;
	}

	public StockBlockProduct removeStockBlockProduct(
			StockBlockProduct stockBlockProduct) {
		getStockBlockProducts().remove(stockBlockProduct);
		stockBlockProduct.setStockBlock(null);

		return stockBlockProduct;
	}

}