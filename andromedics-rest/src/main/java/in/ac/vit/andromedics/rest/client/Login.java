package in.ac.vit.andromedics.rest.client;

import in.ac.vit.andromedics.entities.Employee;
import in.ac.vit.andromedics.entities.Person;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.rest.utils.Utils;
import in.ac.vit.andromedics.security.AndroMedicsPrincipal;
import in.ac.vit.andromedics.service.UserService;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;

@Path("/client")
public class Login {

	@Inject
	private UserService userService;

	private transient Logger log = Logger.getLogger(getClass());

	@POST
	@Path("/login")
	@Produces({ MediaType.APPLICATION_JSON })
	public String AndroidLogin(@Context SecurityContext context,
			@Context HttpServletResponse response) throws IOException {
		try {
			Principal p = context.getUserPrincipal();
			AndroMedicsPrincipal ap = null;
			if (p instanceof AndroMedicsPrincipal) {
				ap = (AndroMedicsPrincipal) p;
				Person person = ap.getUser();
				if (person.getRole().equals(UserRoles.EMPLOYEE)
						|| person.getRole().equals(UserRoles.MANAGER)) {
					Employee employee = (Employee) person;
					Map<String, Object> map = new HashMap<>(3);
					map.put("id", employee.getId());
					map.put("fullname", employee.fullName());
					map.put("storeId", employee.getStore().getId());
					map.put("branchId", employee.getBranch().getId());
					return Utils.getJSON(map);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Login failed");
		}

		response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		return null;
	}

}