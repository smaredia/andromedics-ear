/*
 * 
 */
package in.ac.vit.andromedics.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * The Class JaxRsActivator.
 *
 * @author sahir
 */
@ApplicationPath("/api")
public class JaxRsActivator extends Application {

}