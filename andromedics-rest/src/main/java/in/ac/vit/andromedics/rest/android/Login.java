package in.ac.vit.andromedics.rest.android;

import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.rest.utils.Utils;
import in.ac.vit.andromedics.security.AndroMedicsPrincipal;
import in.ac.vit.andromedics.service.UserService;

import java.security.Principal;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;

@Path("/android")
public class Login {

	@Inject
	private UserService userService;

	private transient Logger log = Logger.getLogger(getClass());

	@POST
	@Path("/login")
	@Produces({ MediaType.APPLICATION_JSON })
	public String AndroidLogin(@Context SecurityContext context) {
		try {
			Principal p = context.getUserPrincipal();
			AndroMedicsPrincipal ap = null;
			if (p instanceof AndroMedicsPrincipal) {
				ap = (AndroMedicsPrincipal) p;
				if (ap.getUser().getRole().equals(UserRoles.CUSTOMER))
					return Utils.getJSON(ap.getUser());
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Login failed");
		}

		return "{\"error\":\"Login Failed\"}";
	}

}