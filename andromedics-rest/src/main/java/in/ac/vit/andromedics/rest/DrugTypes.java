package in.ac.vit.andromedics.rest;

import in.ac.vit.andromedics.entities.DrugType;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.rest.utils.Utils;
import in.ac.vit.andromedics.service.DrugTypeService;

import java.io.IOException;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

@Path("/drugtype")
public class DrugTypes {

	@Inject
	private DrugTypeService drugTypeService;

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ UserRoles.ADMIN, UserRoles.CUSTOMER, UserRoles.EMPLOYEE,
			UserRoles.MANAGER })
	public DrugType getDrugType(@Context HttpServletResponse response,
			@PathParam("id") int id) throws IOException {
		DrugType drugType = this.drugTypeService.getById(id);
		if (drugType == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		} else {
			return drugType;
		}
		return null;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ UserRoles.ADMIN, UserRoles.CUSTOMER, UserRoles.EMPLOYEE,
			UserRoles.MANAGER })
	public String getAll() throws JsonGenerationException,
			JsonMappingException, IOException {
		List<DrugType> drugTypes = this.drugTypeService.getAll();
		return Utils.getJSON(drugTypes);
	}

}
