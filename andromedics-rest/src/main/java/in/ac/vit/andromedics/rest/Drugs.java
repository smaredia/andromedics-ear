package in.ac.vit.andromedics.rest;

import in.ac.vit.andromedics.entities.Drug;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.service.DrugService;

import java.io.IOException;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/drug")
public class Drugs {

	@Inject
	private DrugService drugService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	@RolesAllowed({ UserRoles.ADMIN, UserRoles.CUSTOMER, UserRoles.EMPLOYEE,
			UserRoles.MANAGER })
	public Drug get(@PathParam("id") int id,
			@Context HttpServletResponse response) throws IOException {
		Drug drug = this.drugService.getById(id);
		if (drug == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return null;
		} else {
			return drug;
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ UserRoles.ADMIN, UserRoles.CUSTOMER, UserRoles.EMPLOYEE,
			UserRoles.MANAGER })
	public List<Drug> getAll() {
		return this.drugService.getAll();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("manufacturer/{id}")
	@RolesAllowed({ UserRoles.ADMIN, UserRoles.CUSTOMER, UserRoles.EMPLOYEE,
			UserRoles.MANAGER })
	public List<Drug> getByManufacturer(@PathParam("id") int id) {
		return this.drugService.getByManufacturer(id);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("manufacturer/{manufacturerId}/drugtype/{drugTypeId}")
	@RolesAllowed({ UserRoles.ADMIN, UserRoles.CUSTOMER, UserRoles.EMPLOYEE,
			UserRoles.MANAGER })
	public List<Drug> getByManufacturerAndType(
			@PathParam("manufacturerId") int manufacturerId,
			@PathParam("drugTypeId") int drugTypeId) {
		return this.drugService
				.getByManufacturerAndType(manufacturerId, drugTypeId);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("name/like/{name}")
	@RolesAllowed({UserRoles.ADMIN, UserRoles.CUSTOMER, UserRoles.MANAGER, UserRoles.EMPLOYEE})
	public List<Drug> getNameLike(@PathParam("name") String name) {
		return this.drugService.getNameLike(name);
	}
}
