package in.ac.vit.andromedics.rest.utils;

import org.codehaus.jackson.map.ObjectMapper;

public class Utils {

	public static String getJSON(Object Obj) {
		try {
			return new ObjectMapper().writeValueAsString(Obj);
		} catch (Exception e) {
			return null;
		}
	}
}
