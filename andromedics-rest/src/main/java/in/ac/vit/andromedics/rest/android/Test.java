package in.ac.vit.andromedics.rest.android;

import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.security.AndroMedicsPrincipal;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

@Path("/")
public class Test {

	@Path("/test")
	@POST
	@RolesAllowed({UserRoles.CUSTOMER})
	public String getRole(@Context SecurityContext context) {

		if (context.isUserInRole(UserRoles.CUSTOMER)) {
			AndroMedicsPrincipal p = (AndroMedicsPrincipal) context
					.getUserPrincipal();
			return p.getUser().fullName();
		} else {
			return "No User";
		}
	}

}
