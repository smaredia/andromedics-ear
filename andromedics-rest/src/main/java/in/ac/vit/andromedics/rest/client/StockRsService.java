package in.ac.vit.andromedics.rest.client;

import in.ac.vit.andromedics.entities.Stock;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.entities.form.DrugBean;
import in.ac.vit.andromedics.exceptions.ValidationException;
import in.ac.vit.andromedics.service.StockService;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/stock")
public class StockRsService {

	@Inject
	private StockService stockService;

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ UserRoles.EMPLOYEE, UserRoles.MANAGER })
	public String addStock(List<DrugBean> drugs) {
		try {
			this.stockService.addStock(drugs);
			return "{\"error\":0}";
		} catch (EJBException e) {
			if (e.getCause() instanceof ValidationException) {
				ValidationException ve = (ValidationException) e.getCause();
				return ve.getAsJSON();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	@GET
	@Path("/drug/{drugId}/store/{storeId}/branch/{branchId}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ UserRoles.MANAGER, UserRoles.EMPLOYEE })
	public String getStock(@PathParam("drugId") int drugId,
			@PathParam("storeId") int storeId,
			@PathParam("branchId") int branchId) {
		Stock stock = this.stockService.getByDrug(drugId, storeId, branchId);
		if (stock == null) {
			return "{\"drugId\":" + drugId + ",\"qty\":0}";
		} else {
			return "{\"drugId\":" + drugId + ",\"qty\":" + stock.getQuantity()
					+ "}";
		}
	}
}