package in.ac.vit.andromedics.rest;

import java.io.IOException;
import java.util.List;

import in.ac.vit.andromedics.entities.DrugManufacturer;
import in.ac.vit.andromedics.entities.UserRoles;
import in.ac.vit.andromedics.rest.utils.Utils;
import in.ac.vit.andromedics.service.DrugManufacturerService;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/drugmanufacturer")
public class DrugManufacturers {

	@Inject
	private DrugManufacturerService manufacturerService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ UserRoles.ADMIN, UserRoles.CUSTOMER, UserRoles.EMPLOYEE,
			UserRoles.MANAGER })
	public String getAll() {
		List<DrugManufacturer> manufacturers = this.manufacturerService.getAll();
		return Utils.getJSON(manufacturers);
		
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	@RolesAllowed({ UserRoles.ADMIN, UserRoles.CUSTOMER, UserRoles.EMPLOYEE,
		UserRoles.MANAGER })
	public DrugManufacturer get(@PathParam("id") int id,
			@Context HttpServletResponse response) throws IOException {
		DrugManufacturer manufacturer = this.manufacturerService.getById(id);
		if(manufacturer == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return null;
		} else {
			return manufacturer;
		}
	}
}